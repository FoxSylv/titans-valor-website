/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        "./src/**/*.{js,ts,jsx,tsx,mdx}"
    ],
    theme: {
        extend: {
            fontFamily: {
                minecraft: ["Minecraft", "sans-serif"],
                header: ["Inter", "sans-serif"]
            },
            screens: {
                "xs": "480px",
                "xxs": "360px"
            }
        }
    },
    plugins: [],
};

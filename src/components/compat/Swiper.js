/* This compatability component is required since React 18 does not support web components adequately */

"use client"
import { useEffect, useRef } from 'react';
import { register } from 'swiper/element/bundle';

export function Swiper(props) {
    const swiperRef = useRef(null);
    const {children, ...sprops} = props;

    useEffect(() => {
        register();
        Object.assign(swiperRef.current, {...sprops});
        swiperRef.current.initialize();
    });

    return (
        <swiper-container init="false" ref={swiperRef}>
            {children}
        </swiper-container>
    );
}
export function SwiperSlide(props) {
    const {children, ...sprops} = props;
    return (
        <swiper-slide {...sprops}>
            {children}
        </swiper-slide>
    );
}


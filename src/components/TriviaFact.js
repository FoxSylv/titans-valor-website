/* This is exported as text and not JSX because it's only used in the 
 * news volumes, which dangerously set inner HTML from text. I could not find a
 * better way of making this work.
 */
export default function TriviaFact(props) {
    return (`
        <div class="w-full flex flex-row">
            <div class="w-full flex flex-col">
                <div class="w-[calc(100%-2rem)] m-auto border-t-[0.5rem] border-black dark:border-white">
                </div>
                <div class="w-[calc(100%-1rem)] m-auto h-2 border-x-[0.5rem] border-black dark:border-white">
                </div>
                <div class="w-full border-x-[0.5rem] border-black dark:border-white p-4 font-minecraft text-blue-900 dark:text-blue-400 text-xl">
                    ${props.children}
                </div>
                <div class="w-[calc(100%-1rem)] m-auto h-2 border-x-[0.5rem] border-black dark:border-white">
                </div>
                <div class="w-[calc(100%-2rem)] m-auto flex flex-row">
                    <div class="w-[calc(100%-5.5rem)] border-t-[0.5rem] border-black dark:border-white">
                    </div>
                    <div class="w-12">
                    </div>
                    <div class="w-10 border-t-[0.5rem] border-black dark:border-white">
                    </div>
                </div>
                <div class="w-16 h-2 border-x-[0.5rem] border-black dark:border-white ml-[calc(100%-7rem)]">
                </div>
                <div class="w-16 h-2 border-x-[0.5rem] border-black dark:border-white ml-[calc(100%-7rem)]">
                </div>
                <div class="w-[4.5rem] h-2 border-x-[0.5rem] border-black dark:border-white ml-[calc(100%-7rem)]">
                </div>
                <div class="w-[4.5rem] h-2 border-x-[0.5rem] border-black dark:border-white ml-[calc(100%-6.5rem)]">
                </div>
                <div class="w-16 border-t-[0.5rem] border-black dark:border-white ml-[calc(100%-6rem)]">
                </div>
                <div class="w-full h-0 hidden lg:inline">
                    <div class="ml-auto w-40 -translate-y-12 translate-x-24 relative -z-10">
                        <Image src="/news/triviapose.png" alt="" width="100%" />
                        <div class="w-full h-full absolute left-0 top-0 pointer-events-none bg-gradient-to-t from-white dark:from-slate-950 to-20%">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `);
}
 

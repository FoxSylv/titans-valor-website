export default function MinecraftSkinLoader({src, width, quality}) {
    return `https://visage.surgeplay.com/full/512/${src}`;
}

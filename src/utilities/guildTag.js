import dbQuery from '../data/dbQuery.js';
import Cache from '../utilities/Cache.js';

const cache = new Cache();
async function getGuildList() {
    return await cache.get("data", async () => await dbQuery(`
        SELECT guild, tag
        FROM guild_tag_name
        ORDER BY priority DESC
    `));
}

export async function guildTagToName(tag) {
    return (await getGuildList()).find(guild => guild.tag?.toLowerCase() === tag.toLowerCase())?.guild;
}
export async function guildNameToTag(name) {
    return (await getGuildList()).find(guild => guild.guild?.toLowerCase() === name.toLowerCase())?.tag;
}
export async function getGuildData(info) {
    return (await getGuildList()).find(guild => guild.guild?.toLowerCase() === info.toLowerCase() || guild.tag?.toLowerCase() === info.toLowerCase());
}

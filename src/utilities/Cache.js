/* Map stores by insertion order, allowing us to have LRU-like behaviour */
export default class Cache {
    constructor(timeLimit = 86400000, max = Number.MAX_SAFE_INTEGER) {
        this.timeLimit = timeLimit;
        this.max = max;
        this.cache = new Map();
    }

    #clearOldCacheValues() {
        this.cache.forEach((entry, key) => {
            if (entry.timestamp < (Date.now() - this.timeLimit)) {
                this.cache.delete(key);
            }
        });
    }


    async get(key, fallback = async (key) => undefined) {
        this.#clearOldCacheValues();

        let result = this.cache.get(key);
        if (result !== undefined) {
            this.cache.delete(key);
            this.cache.set(key, result);
            result = result.value;
        }
        else {
            result = await fallback(key);
            if (result !== undefined) {
                this.cache.set(key, {value: result, timestamp: Date.now()});
            }
        }
        return result;
    }

    set(key, value) {
        this.#clearOldCacheValues();

        let timestamp = Date.now();
        if (this.cache.has(key)) {
            timestamp = this.cache.get(key).timestamp;
            this.cache.delete(key);
        }
        else if (this.cache.size >= this.max) {
            this.cache.delete(this.cache.keys().next().value);
        }
        this.cache.set(key, {value: value, timestamp: timestamp});
        return value;
    }
}

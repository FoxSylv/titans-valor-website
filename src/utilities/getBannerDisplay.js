import Image from 'next/image';

const bannerColors = [
    {name: "WHITE", color: "bg-[#FFFFFF]"},
    {name: "BLACK", color: "bg-[#191919]"},
    {name: "GRAY", color: "bg-[#4C4C4C]"},
    {name: "SILVER", color: "bg-[#999999]"},
    {name: "PINK", color: "bg-[#F27FA5]"},
    {name: "MAGENTA", color: "bg-[#B24CD8]"},
    {name: "PURPLE", color: "bg-[#7F3FB2]"},
    {name: "BLUE", color: "bg-[#334CB2]"},
    {name: "CYAN", color: "bg-[#4C7F99]"},
    {name: "LIGHT_BLUE", color: "bg-[#6699D8]"},
    {name: "GREEN", color: "bg-[#667F33]"},
    {name: "LIME", color: "bg-[#7FCC19]"},
    {name: "YELLOW", color: "bg-[#E5E533]"},
    {name: "ORANGE", color: "bg-[#D87F33]"},
    {name: "BROWN", color: "bg-[#664C33]"},
    {name: "RED", color: "bg-[#993333]"}
];
function getBannerColor(color, isBase = false) {
    return bannerColors.find(c => c.name === color)?.color ?? "bg-transparent";
}


export default function getBannerDisplay(banner, alt) {
    return (
        <div role="img" alt={alt ?? ""} className={`w-full h-full relative ${getBannerColor(banner.base, true)}`}>
            {banner.layers.map((layer, index) => {
                const bannerURL = `https://beta-cdn.wynncraft.com/nextgen/banners/${layer.pattern}.svg`;
                return (
                    <div key={`${index}-${layer.pattern}`} className="absolute w-full h-full top-0 right-0">
                        <style> {/* This is needed to make image masking work :( */}
                            {`.banner-${layer.pattern} { -webkit-mask-image: url(${bannerURL}); -webkit-mask-position: center; -webkit-mask-repeat: no-repeat; mask-image: url(${bannerURL}) center center no-repeat}`}
                        </style>
                        <div className={`${getBannerColor(layer.colour)} w-full h-full banner-${layer.pattern}`} />
                    </div>
                );
            })}
        </div>
    );
}

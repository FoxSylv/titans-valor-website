import fs from 'fs';

function getGuildIconList() {
    const iconDir = `${process.cwd()}/public/guild-icons`;
    try {
        return fs.readdirSync(iconDir);
    }
    catch(e) {
        console.log(e);
        return [];
    }
}
const guildIcons = getGuildIconList();
export default guildIcons;

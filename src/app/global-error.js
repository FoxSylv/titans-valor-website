'use client'

export default function Error({ error, reset }) {
    return (
        <div className="mt-48 text-center">
            <h2>
                Something went wrong!
            </h2>
            <button onClick={() => reset()} className="w-36 m-auto py-3 mt-12 text-white rounded-lg shadow-sm bg-black dark:bg-zinc-800 hover:bg-slate-800 focus:slate-800 transition-all duration-300">
                <strong>
                    Try Again
                </strong>
            </button>
        </div>
    );
}

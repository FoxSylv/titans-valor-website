"use client"
import { useState } from 'react';
import Image from 'next/image';
import MinecraftSkinLoader from '../utilities/MinecraftSkinLoader.js';


export default function Leaderboard(props) {
    const tabs = [{data: props.topXP, dataName: "xp", affix: "XP"}, {data: props.topWars, dataName: "wars", affix: "Wars"}, {data: props.topActivity, dataName: "activity", affix: "Hours"}];
    let [currentTab, setCurrentTab] = useState(0);

    const buttons = ["Guild XP", "Wars", "Activity"].map((tabName, tabNum) => {
        return (
            <button key={`${tabName} Button`} onClick={() => setCurrentTab(tabNum)} className={`py-2 px-4 rounded-md transition-all duration-200 ${currentTab === tabNum ? "bg-zinc-800" : "bg-zinc-800/40 hover:bg-zinc-800/60 hover:bg-zinc-800/60"}`}>
                <p className="my-0">
                    {tabName}
                </p>
            </button>
        );
    });

    const playerDisplays = tabs[currentTab].data.map((player, playerNum) => {
        const smallNameSizes = "text-base xs:text-lg sm:text-2xl lg:text-[2rem] py-1";
        const bigNameSizes = "text-[0.6rem] xs:py-1 xs:text-base sm:text-xl lg:text-[2rem]";
        const positionClasses = ["", "order-first mt-12", "mt-12"];
        const rankColors = ["text-amber-400", "text-neutral-400", "text-orange-700"];
        return (
            <div key={`${playerNum + 1}${tabs[currentTab].affix}`} className={`md:w-64 ${positionClasses[playerNum]}`}>
                <h4 className={`inline-block ${player.name.length < 10 ? smallNameSizes : bigNameSizes} font-minecraft font-normal bg-black/30 px-2 mb-4`}>
                    {player.name}
                </h4>
                <Image loader={MinecraftSkinLoader} src={player.uuid} alt={`${player.name}'s Minecraft Skin`} width="170" height="300" className="m-auto"/>
                <h5 className={`mt-4 text-5xl ${rankColors[playerNum]}`}>
                    {`#${playerNum + 1}`}
                </h5>
                <p className="text-xs xs:text-sm md:text-base mt-2 font-bold">
                    {`${parseInt(player[tabs[currentTab].dataName]).toLocaleString()} ${tabs[currentTab].affix}`}
                </p>
            </div>
        );
    });


    return (
        <div className="text-center pb-48 text-white mx-4">
            <h1 className="text-black">
                Top Weekly Players
            </h1>
            <div className="flex justify-center gap-2 md:gap-8 m-auto mt-8">
                {buttons}
            </div>
            <div className="flex justify-center gap-2 md:gap-8 m-auto mt-8">
                {playerDisplays}
            </div>
        </div>
    );
}

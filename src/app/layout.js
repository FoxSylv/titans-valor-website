import "./globals.css";
import Navbar from './Navbar';

export const metadata = {
    title: "Titan's Valor",
    description: "The online home for the Wynncraft guild Titan's Valor! Welcome in!",
};

export default function RootLayout({ children }) {
    return (
        <html lang="en">
            <body>
                <Navbar /> 
                {children}
            </body>
        </html>
    );
}

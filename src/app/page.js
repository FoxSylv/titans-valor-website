import Image from 'next/image';
import Link from 'next/link';
import newsData from '../data/newsData.json';
import { Swiper, SwiperSlide } from '../components/compat/Swiper';
import Cache from '../utilities/Cache.js';
import dbQuery from '../data/dbQuery.js';
import Leaderboard from './Leaderboard';


export const metadata = {
    title: "Welcome to Titans Valor!",
    description: "The homepage for the Wynncraft guild Titans Valor. Enter the heart of the sea and enjoy your stay!"
};
export const dynamic = 'force-dynamic'


const cache = new Cache(1200000, 6);

export default async function Home() {
    const currentTime = (900 * Math.floor(Date.now() / 900000)) - 604800;
    const [topWars, topXP, topActivity] = await Promise.all([
        cache.get(`wars-${currentTime}`, async () => await dbQuery(`
            SELECT uuid_name.uuid, uuid_name.name,
            SUM(delta_warcounts.warcount_diff) AS wars
            FROM delta_warcounts
            LEFT JOIN uuid_name ON uuid_name.uuid=delta_warcounts.uuid
            LEFT JOIN player_stats ON player_stats.uuid=delta_warcounts.uuid
            WHERE player_stats.guild = 'Titans Valor'
            AND delta_warcounts.time > ${currentTime}
            GROUP BY uuid_name.uuid
            ORDER BY wars DESC
            LIMIT 3
        `)),
        cache.get(`xp-${currentTime}`, async () => await dbQuery(`
            SELECT uuid_name.uuid, uuid_name.name,
            SUM(player_delta_record.delta) AS xp
            FROM player_delta_record
            LEFT JOIN uuid_name ON uuid_name.uuid = player_delta_record.uuid
            LEFT JOIN guild_member_cache ON guild_member_cache.name = uuid_name.name
            WHERE player_delta_record.label = "gu_gxp"
            AND guild_member_cache.guild = "Titans Valor"
            AND player_delta_record.time > ${currentTime}
            GROUP BY uuid_name.name, uuid_name.uuid
            ORDER BY xp DESC
            LIMIT 3
        `)),
        cache.get(`activity-${currentTime}`, async () => await dbQuery(`
            SELECT uuid, name,
            COUNT(name) AS activity
            FROM activity_members
            WHERE guild = 'Titans Valor'
            AND timestamp > ${currentTime}
            GROUP BY uuid, name
            ORDER BY activity DESC
            LIMIT 3
        `))
    ]);


    const newsPreviews = newsData.map((volume, volNum) => {
        return <SwiperSlide key={`newsPreview.${volume.link}.${volNum}`}>
            <div className="h-96 p-5 mx-2 rounded-md bg-white dark:bg-zinc-800 text-black dark:text-zinc-200">
                <p className="text-xs text-left">
                    {`📅 ${volume.publicationDate ?? "Date Not Found"}`}
                </p>
                <h2 className="text-2xl font-bold mt-4 mb-2">
                    {`Volume ${volNum + 1}`}
                </h2>
                <div className="h-48 overflow-hidden relative">
                    <p className="text-left">
                        {volume.preamble}
                    </p>
                    <div className="w-full h-2/3 absolute bottom-0 bg-gradient-to-t from-white dark:from-zinc-800">
                    </div>
                </div>
            <Link href={`/news/${volume.link}`} className="block w-full mt-4 py-3 font-bold text-white rounded-md shadow-sm bg-black hover:bg-slate-800 hover:slate-800 transition-all duration-300" draggable="false">
                    Read More
                </Link>
            </div>
        </SwiperSlide>;
    });


    return (<>
        <main id="main">
            <section className="mt-16 pb-8 w-full">
                <div className="flex items-center mt-24 max-w-6xl px-8 m-auto">
                    <div className="flex-grow md:max-w-lg">
                        <h1 className="mb-12">
                            About the Guild
                        </h1>
                        <p>
                            Titans Valor, due to its extensive and rich history as a dominant guild during times of both prosperity and hardship, is an exemplar of perseverance and resilience among guilds.
                            Titans Valor does not crumble in times without territories, nor does it grow stagnant when fortune favors the guild.
                            Rather, Titans Valor consistently remains an active, unrelenting force that persists in spite of the ever-changing guild climate.
                        </p>
                    </div>
                    <div className="hidden md:inline min-w-[20rem]">
                        <Image src="/ANO.png" width={600} height={600} alt="" />
                    </div>
                </div>
            </section>
            <section className="w-full min-h-[100vh] bg-[url(/viking-nations.png)] bg-center bg-cover bg-no-repeat relative">
                <div className="mt-16 max-w-6xl pt-32 m-auto">
                    <Leaderboard topWars={topWars} topXP={topXP} topActivity={topActivity} />
                </div>
                <div className="w-full h-full absolute left-0 top-0 pointer-events-none bg-gradient-to-b from-white dark:from-black to-10%">
                </div>
                <div className="w-full h-full absolute left-0 bottom-0 pointer-events-none bg-gradient-to-t from-black to-10%">
                </div>
            </section>
            <section className="bg-black text-white p-2 pt-48 w-full text-center relative">
                <h1>
                    Titan Times
                </h1>
                <p className="m-auto mt-12 max-w-lg">
                    Our guild newspaper! We need more writers for it!
                    If you&apos;re interested in interviewing members of the guild,
                    writing short stories, and informing the rest of the guild about whats going on,
                    this is what you should look into!
                </p>
                <div className="max-w-screen-2xl mt-32 mx-auto relative">
                    <Swiper breakpoints={{"0": {"slidesPerView": "1.2"}, "640": {"slidesPerView": "3.3"}, "1280": {"slidesPerView": "5.4"}}} centeredSlides={true} grabCursor={true} initialSlide={newsData.length}>
                        {newsPreviews}
                    </Swiper>
                    <div className="w-1/4 h-full absolute left-0 bottom-0 pointer-events-none bg-gradient-to-r from-black to-20% sm:to-10% z-10">
                    </div>
                    <div className="w-1/4 h-full absolute right-0 bottom-0 pointer-events-none bg-gradient-to-l from-black to-20% sm:to-10% z-10">
                    </div>
                </div>
            </section>
            <section className="bg-black text-white text-center pt-36">
                <div className="w-full min-h-[100vh] bg-[url(/ano-group-photo.png)] bg-center bg-cover bg-no-repeat relative p-2 pt-24">
                    <h1 className="mb-12">
                        Join Our Ranks
                    </h1>
                    <p>
                        Are you looking to join a fun, interactive guild to spice up your Wynncraft experience?
                    </p>
                    <p>
                        Join <strong>Titan&apos;s Valor [ANO]</strong> today! You can apply on our discord server.
                    </p>
                    <div className="w-full h-full absolute left-0 top-0 pointer-events-none bg-gradient-to-b from-black to-10%">
                    </div>
                </div>
            </section>
        </main>
    </>);
}


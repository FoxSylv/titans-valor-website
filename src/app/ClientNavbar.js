'use client';

import Link from 'next/link';
import Image from 'next/image';
import { useState, useEffect, useRef } from 'react';
import Header from '../components/Header';
import newsData from '../data/newsData.json';

function SearchButton(props) {
    return (
        <button title="Search" className={props.className ?? ""} onClick={() => props.openSearch()} tabIndex={props.isNavigable ? "0" : "-1"} ref={props.ref}>
            <Image src="/search.svg" width={props.width ?? 29} height={props.height ?? 29} alt="Search Button" />
        </button>
    );
}
function SearchResult(props) {
    return (
        <li className="my-2 ml-0 p-2 hover:bg-zinc-200 focus:bg-zinc-200 dark:hover:bg-zinc-800 dark:focus:bg-zinc-800 transition-all duration-300 rounded-md" >
            <Link href={`/news/${props.result.ref}`} onClick={() => props.closeSearch()}>
                <h5 className="mt-0">
                    {`Volume ${props.result.ref.slice(3)}`}
                </h5>
                <blockquote className="my-0 h-20 overflow-hidden relative z-50">
                    <p className="mt-2">
                        {props.newsRegistry.find(v => v.vol === props.result.ref).preamble}
                    </p>
                </blockquote>
            </Link>
        </li>
    );
}
function SearchResults(props) {
    const results = undefined ?? [];
    const resultList = results.map(r => <SearchResult key={r.ref} result={r} closeSearch={props.closeSearch} newsRegistry={props.newsRegistry} />);

    if (resultList.length === 0) {
        return (
            <p className="w-full text-center mt-12">
                <em>
                    No Results Found! :(
                </em>
            </p>
        );
    }
    return (
        <ol className="list-none mb-20">
            {resultList}
        </ol>
    );
}

function NavLinks(props) {
    return (
        <nav className={props.className}>
            <Link href={`/news/${newsData.at(-1).link}`} title="News" className={props.linkClassName} tabIndex={props.isNavigable ? "0" : "-1"}>
                <h6 className="mt-0">
                    News
                </h6>
            </Link>
            <Link href="/leaderboard" title="Leaderboard" className={props.linkClassName} tabIndex={props.isNavigable ? "0" : "-1"}>
                <h6 className="mt-0">
                    Leaderboard
                </h6>
            </Link>
            <Link href="/map" title="Leaderboard" className={props.linkClassName} tabIndex={props.isNavigable ? "0" : "-1"}>
                <h6 className="mt-0">
                    Map
                </h6>
            </Link>
            <Link href="https://discord.gg/h8XUHkR" title="Titan's Valor Discord" className={props.linkClassName} target="_blank" rel="noopener noreferrer" tabIndex={props.isNavigable ? "0" : "-1"}>
                <h6 className="mt-0">
                    Apply
                </h6>
            </Link>
        </nav>
    );
}

export default function ClientNavbar(props) {
    /* Scroll handling (to expand when stickied) */
    const [scrollPosition, setScrollPosition] = useState(0);
    useEffect(() => {
        let scrollHandler = () => setScrollPosition(window.scrollY);
        window.addEventListener('scroll', scrollHandler, {passive: true});
        return () => window.removeEventListener('scroll', scrollHandler);
    }, []);

    /* Navigation */
    let [isSubOpen, setIsSubOpen] = useState(false);
    const buttonClasses = "rounded-lg transition-all duration-300 hover:bg-zinc-800 focus:bg-zinc-800";

    /* Search Popup */
    let [isSearchOpen, setIsSearchOpen] = useState(false);
    let [searchQuery, setSearchQuery] = useState("");
    

    return (<>
        {/* Background dimming */}
        <div className="sticky top-0 left-0 w-0 h-0 z-40">
            <div className={`w-[100vw] h-[100vh] backdrop-blur-sm bg-white/30 transition-all duration-300 ${(isSubOpen || isSearchOpen) ? "bg-transparent" : "hidden"}`}>
            </div>
        </div>

        {/* Main header*/}
        <div className="sticky top-0 w-full h-0 z-30">
            <div style={{"paddingTop": Math.max(0, 24 - scrollPosition)}}></div> {/* I hate this but it's the only way i could get the sidebar to not have a gap above it */}
            <Header height="4.375" hasFlares={scrollPosition <= 24} className="shadow-xl">
                <div className="h-full md:pl-4 flex">
                    <div className="h-full px-4 flex items-center">
                        <Link href="/" title="Homepage" aria-label="Homepage" className={`flex items-center transition-all duration-300 lg:mr-12 pl-2 pr-4 rounded-2xl ${buttonClasses}`}>
                            <Image src="/ANO.png" width={80} height={40} priority={true} alt="Titan's Valor logo" aria-hidden="true"/>
                            <h3 className="font-minecraft hidden xs:inline mt-2 ml-3" aria-hidden="true">
                                [ANO]
                            </h3>
                        </Link>
                        <NavLinks newsRegistry={props.newsRegistry} className="hidden lg:flex items-center gap-6 mr-8" linkClassName={`px-4 py-1 ${buttonClasses}`} isNavigable={true}/>
                    </div>
                    <div className="flex-grow flex flex-row-reverse items-center">
                        <SearchButton className={`${buttonClasses} p-3 mr-2 hidden lg:inline`} openSearch={() => setIsSearchOpen(true)} isNavigable={true} />
                        <button title="Navigate" className={`${buttonClasses} p-2 mr-2 inline lg:hidden`} onClick={() => setIsSubOpen(!isSubOpen)}>
                            <svg width="2rem" height="2rem" viewBox="0 0 24 24" alt="Navigate Button">
                                <path d="M5 6h14M5 12h14M5 18h14" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round"></path>
                            </svg>
                        </button>
                    </div>
                </div>
            </Header>
        </div>

        {/* Navigation sidebar for smaller screens */}
        <div className="sticky top-0 left-0 w-0 h-0 z-50 lg:hidden">
            <div className={`min-w-[75vw] xs:min-w-[50vw] h-[100vh] bg-zinc-950 dark:bg-zinc-900 transition-all duration-300 ${(isSubOpen && !isSearchOpen) ? "" : "-translate-x-[100%]"}`}>
                <div className="p-6 flex">
                    <button onClick={() => setIsSubOpen(!isSubOpen)} tabIndex={(isSubOpen && !isSearchOpen) ? "0" : "-1"}>
                        <Image src="/arrow.svg" width={29} height={29} alt="Return Button" className="invert rotate-90 transition-all hover:-translate-x-1" />
                    </button>
                    <div className="flex-grow flex flex-row-reverse">
                        <SearchButton className={`p-2 ${buttonClasses}`} openSearch={() => setIsSearchOpen(true)} isNavigable={isSubOpen && !isSearchOpen} />
                    </div>
                </div>
                <NavLinks newsRegistry={props.newsRegistry} className="mt-4 flex flex-col" linkClassName="p-4 font-header font-semibold text-white dark:text-slate-200 text-2xl odd:bg-zinc-900 dark:odd:bg-zinc-950 hover:bg-zinc-800 focus:bg-zinc-800 transition-all duration-300" isNavigable={isSubOpen && !isSearchOpen} />
            </div>
        </div>

        {/* Search Popup */}
        <div className="sticky top-0 left-0 w-0 h-0 z-50">
            <div className={`w-[100vw] h-[100vh] pt-16 ${isSearchOpen ? "" : "hidden"}`}>
                <div className="w-full max-w-lg mx-auto mt-16 p-4">
                    <div className="w-full h-[calc(75vh-4rem)] bg-white dark:bg-zinc-950 border-2 border-zinc-300 rounded-lg shadow-2xl relative">
                        <button className="absolute top-4 left-4 transition-all hover:-translate-x-1" onClick={() => setIsSearchOpen(false)}>
                            <Image src="/arrow.svg" width={35} height={35} alt="Return Button" className="rotate-90 invert-0 dark:invert" />
                        </button>
                        <h2 className="text-center">
                            Search
                        </h2>
                        <div className="mx-6 mt-4">
                            <input type="text" placeholder="Search..." title="Type search query here" role="searchbox" aria-label="Search Box" className="w-full p-2 text-xl bg-zinc-100 dark:bg-zinc-800" onInput={e => setSearchQuery(e.target.value)} />
                        </div>
                        <div className="border-2 border-zinc-300 rounded-lg mx-4 mt-4"></div>
                        <div className="h-[calc(75vh-16rem)] mx-4 pl-2 pr-6 mt-4 overflow-scroll relative" tabIndex="-1">
                            <div className="sticky top-0 left-0 w-full h-0 z-[60]">
                                <div className="absolute top-0 left-0 w-full h-[calc(75vh-16rem)] pointer-events-none bg-gradient-to-b from-white dark:from-black to-10%"></div>
                                <div className="absolute top-0 left-0 w-full h-[calc(75vh-16rem)] pointer-events-none bg-gradient-to-t from-white dark:from-black to-20%"></div>
                            </div>
                            <SearchResults idx={undefined} query={searchQuery} closeSearch={() => setIsSearchOpen(false)} newsRegistry={props.newsRegistry} />
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {/* Spacing (So things don't appear behind the Navbar) */}
        <div className="w-full h-24"></div>
    </>);
}

'use server'
import Link from 'next/link';
import dbQuery from '../../data/dbQuery.js';
import Cache from '../../utilities/Cache.js';
import { getGuildData } from '../../utilities/guildTag.js';
import guildIcons from '../../data/guildIcons.js';
import getBannerDisplay from '../../utilities/getBannerDisplay.js';
import { EMPTY_LB_TYPE } from './buttonData.js';


const rankingNames = {
    playerContent: "Character Content Completion",
    globalPlayerContent: "Global Content Completion",
    combatSoloLevel: "Character Combat Level",
    combatGlobalLevel: "Global Combat Level",
    totalSoloLevel: "Character Total Level",
    totalGlobalLevel: "Global Total Level",
    professionsSoloLevel: "Character Professions Level",
    professionsGlobalLevel: "Global Professions Level",
    fishingLevel: "Fishing",
    woodcuttingLevel: "Woodcutting",
    miningLevel: "Mining",
    farmingLevel: "Farming",
    scribingLevel: "Scribing",
    jewelingLevel: "Jeweling",
    alchemismLevel: "Alchemism",
    cookingLevel: "Cooking",
    weaponsmithingLevel: "Weaponsmithing",
    tailoringLevel: "Tailoring",
    woodworkingLevel: "Woodworking",
    armouringLevel: "Armouring",
    warsCompletion: "Wars",
    nogCompletion: "Nest of Grootslangs",
    tccCompletion: "The Canyon Colossus",
    nolCompletion: "Nexus of Light",
    tnaCompletion: "The Nameless Anomaly",
    hardcoreLegacyLevel: "Legacy Hardcore Level",
    ironmanContent: "Ironman Content Completion",
    ultimateIronmanContent: "Ultimate Ironman Content Completion",
    hardcoreContent: "Hardcore Content Completion",
    craftsmanContent: "Craftsman Content Completion",
    huntedContent: "Hunted Content Completion",
    huicContent: "HUIC Content Completion",
    huichContent: "HUICH Content Completion",
    hichContent: "HICH Content Completion",
    hicContent: "HIC Content Completion"
};

const ranks = ["recruit", "recruiter", "captain", "strategist", "chief", "owner"];
function getGuildMemberList(guildInfo) {
    return ranks.reduce((members, rank) => members.concat(Object.keys(guildInfo?.members?.[rank] ?? {})));
}

const cache = new Cache(1000000, 300);
export default async function fetchStats(searchQuery, leaderboardType, isCorrupt, timestamp, grouping) {
    /* Determine guild or user search behaviour */
    if (searchQuery === "") {
        searchQuery = "ANO";
    }
    const guildData = await getGuildData(searchQuery);
    const isGuild = guildData !== undefined;
    let name = isGuild ? guildData.guild : searchQuery;

    /* Get status */
    let online;
    let uuid;
    let playerGuildName;
    let guildTag;
    let hasGuildIcon;
    let guildBanner;
    let guildMemberList;
    if (isGuild) {
        const endpoint = `https://api.wynncraft.com/v3/guild/${name}`;
        let guildInfo = await cache.get(endpoint, async () => await fetch(endpoint, {cache: "no-store"}).then(response => response.json()));
        if (!Object.keys(guildInfo).includes("uuid")) { /* If the Wynn API has multiple unique guilds with the same name (NOT COMMON) */
            const newEndpoint = `https://api.wynncraft.com/v3/guild/prefix/${Object.keys(guildInfo).at(-1)}`;
            guildInfo = await cache.get(newEndpoint, async () => await fetch(newEndpoint, {cache: "no-store"}).then(response => response.json()));
        }
        online = guildInfo.online;
        uuid = guildInfo.uuid;
        guildMemberList = getGuildMemberList(guildInfo);
        guildTag = guildData?.tag ?? name;
        hasGuildIcon = guildIcons.includes(guildTag);
        guildBanner = guildInfo?.banner ?? [];

        if (leaderboardType.name === EMPTY_LB_TYPE.name) {
            let stats = ranks.reduce((members, rank, stars) => {
                return members.concat(Object.keys(guildInfo?.members?.[rank] ?? {}).reduce((members, member) => {
                    return members.concat({left: "★".repeat(stars), middle: <Link href={`https://www.wynncraft.com/stats/player/${guildInfo.members[rank][member].uuid}`} className="hover:italic focus:italic">
                {member}
            </Link>, right: guildInfo.members[rank][member].server ?? ""});
                }, []));
            }, []);
            const creationDate = (new Date(guildInfo?.created)).toLocaleDateString();
            stats = stats.concat([
                {left: "", middle: "", right: ""},
                {left: "", middle: `${guildInfo?.members?.total ?? 0} Members`, right: ""},
                {left: `Level ${guildInfo?.level ?? 0}.${guildInfo?.xpPercent ?? 0}`, middle: "", right: `${guildInfo?.territories ?? 0} Territories`},
                {left: "", middle: creationDate === "Invalid Date" ? "Creation Date Not Found" : `Created ${creationDate}`, right: ""}
            ]);
            stats.reverse();
            return {isValid: true, isGuild: true, tag: guildTag, hasGuildIcon: hasGuildIcon, guildBanner: guildBanner, name: name, uuid: uuid, online: online, stats: stats, isGuildStats: true};
        }
    }
    else {
        const endpoint = `https://api.wynncraft.com/v3/player/${name}`;
        let playerInfo = await cache.get(endpoint, async () => await fetch(endpoint, {cache: "no-store"}).then(response => response.json()));
        if (!Object.keys(playerInfo).includes("uuid")) { /* If the Wynn API has multiple unique players with the same username (NOT COMMON) */
            const newEndpoint = `https://api.wynncraft.com/v3/player/${Object.keys(playerInfo).at(-1)}`;
            playerInfo = await cache.get(newEndpoint, async () => await fetch(newEndpoint, {cache: "no-store"}).then(response => response.json()));
        }
        online = playerInfo.online ? playerInfo.server.slice(2) : 0;
        uuid = playerInfo.uuid;
        name = playerInfo.username ?? name; /* To get correct capitalization */
        playerGuildName = playerInfo.guild?.name ?? "None";
        if (playerGuildName === "None" && grouping.name === "Guild") {
            return {isValid: false, error: uuid === undefined ? "Error! Player not found: " : "Error! Not in guild: ", name: name, uuid: uuid};
        }

        if (leaderboardType.name === EMPTY_LB_TYPE.name) {
            const joinDate = (new Date(playerInfo?.firstJoin)).toLocaleDateString();
            const header = [
                {left: "", middle: joinDate === "Invalid Date" ? "Join Date Not Found" : `Joined ${joinDate}`, right: ""},
                {left: playerInfo?.guild?.rankStars ?? "No Guild", middle: playerInfo?.guild?.uuid ? <>{playerInfo?.guild?.rank} of <Link href={`https://www.wynncraft.com/stats/guild/${playerInfo?.guild?.name}`} className="hover:italic focus:italic">{playerInfo?.guild?.name}</Link></> : "", right: ""},
                {left: "", middle: "", right: ""},
                {left: "", middle: "", right: ""}
            ];
            const rankings = Object.keys(playerInfo?.ranking ?? {}).map(ranking => {
                const rankingDiff = playerInfo.ranking[ranking] - playerInfo.previousRanking[ranking];
                let rankingIndicator = ""; 
                if (rankingDiff < 0) {
                    rankingIndicator = `▲${rankingDiff.toLocaleString()}`;
                }   
                if (rankingDiff > 0) {
                    rankingIndicator = `⯆${rankingDiff.toLocaleString()}`;
                }
                const rankStr = `#${playerInfo.ranking[ranking].toLocaleString()}`;
                return {left: playerInfo.ranking[ranking] === 1 ? <strong>{rankStr}</strong> : rankStr, middle: rankingNames[ranking] ?? ranking, right: rankingIndicator};
            });
            rankings.sort((a,b) => a.ranking - b.ranking);
            return {isValid: true, name: name, uuid: uuid, online: online, stats: header.concat(rankings)};
        }
    }

    /* Calculate leaderboard */
    let stats = [];

    const isGuildStats = isGuild && (grouping?.name === "Wynn");
    const baseTable = (timestamp.name === "Total") ? "player_global_stats" : "player_delta_record";
    let searchTerm = "";
    if (isGuild && grouping.name === "ANO") {
        searchTerm = `AND (guild_member_cache.guild = "${name}" OR guild_member_cache.guild = "Titans Valor")`;
    }
    else if (isGuild && grouping.name === "Guild") {
        searchTerm = `AND guild_member_cache.guild = "${name}"`;
    }
    else if (!isGuild && grouping.name !== "Wynn") {
        searchTerm = `AND (guild_member_cache.guild = "${grouping.name === "Guild" ? playerGuildName : "Titans Valor"}" OR ${baseTable}.uuid = "${uuid}")`;
    }

    const query = `
        SELECT ${isGuildStats ? "guild_member_cache.guild AS name" : "uuid_name.name, uuid_name.uuid"},
        ${timestamp.name === "Total" ? "SUM(player_global_stats.value) AS stat" : "SUM(player_delta_record.delta) AS stat"}
        FROM ${baseTable}
        LEFT JOIN uuid_name ON uuid_name.uuid = ${baseTable}.uuid
        LEFT JOIN guild_member_cache ON guild_member_cache.name = uuid_name.name
        WHERE ${baseTable}.label = "${((isCorrupt ? leaderboardType.dbCorruptName : undefined) ?? leaderboardType.dbName) ?? leaderboardType.name}"
        ${timestamp.name === "Total" ? "" : `AND player_delta_record.time > ${(900 * Math.floor(Date.now() / 900000)) - timestamp.time}`}
        ${searchTerm}
        GROUP BY ${isGuildStats ? "guild_member_cache.guild" : "uuid_name.name, uuid_name.uuid"}
        ORDER BY stat DESC
    `;
    stats = await cache.get(query, async () => await dbQuery(query));

    stats = stats.filter(record => record.name !== null);
    if (isGuild && grouping.name === "ANO" && guildData.tag !== "ANO") {
        stats = stats.map(player => {return {name: `[${guildMemberList.includes(player.name) ? guildData.tag : "ANO"}] ${player.name}`, uuid: player.uuid, stat: player.stat}});
    }

    let placement = -1;
    let placementOffset = 0;
    if (!isGuild || (isGuild && grouping.name === "Wynn")) {
        placement = stats.findIndex(record => record.name === name);
        if (placement === -1 && stats.length > 0) { /* If not in leaderboard */
            placement = stats.length;
            stats.push({name: name, uuid: uuid, stat: 0});
        }
    }
    if (placement > 1500) {
        if ((placement > 2500) && (placement > (stats.length - 2000))) {
            placementOffset = stats.length - 2000;
        }
        else {
            placementOffset = placement - 1000;
        }
    }
    stats = stats.slice(placementOffset, placementOffset + 2000);

    stats = stats.map((stat, index) => {
        const link = <Link href={isGuildStats ? `https://www.wynncraft.com/stats/guild/${stat.name}` : `https://www.wynncraft.com/stats/player/${stat.uuid}`} className="hover:italic focus:italic">
                {stat.name}
            </Link>
        return {left: `${(placementOffset + index + 1).toLocaleString()})`, middle: link, right: parseInt(stat.stat).toLocaleString()};
    });


    return {isValid: true, isGuild: isGuild, tag: guildTag, hasGuildIcon: hasGuildIcon, guildBanner: guildBanner, name: name, uuid: uuid, online: online, stats: stats, placement: placement - placementOffset, isGuildStats: isGuildStats};
}

const EMPTY_LB_TYPE = {name: "No List Selected"};
const typeButtonData = [
    {header: "Dungeons", data: [
        {name: "Decrepit Sewers", dbName: "g_Decrepit Sewers", canCorrupt: true, dbCorruptName: "g_Corrupted Decrepit Sewers"},
        {name: "Infested Pit", dbName: "g_Infested Pit", canCorrupt: true, dbCorruptName: "g_Corrupted Infested Pit"},
        {name: "Underworld Crypt", dbName: "g_Underworld Crypt", canCorrupt: true, dbCorruptName: "g_Corrupted Underworld Crypt"},
        {name: "Timelost Sanctum", dbName: "g_Timelost Sanctum", canCorrupt: true, corruptName: "Lost Sanctuary", dbCorruptName: "g_Corrupted Lost Sanctuary"}, /* Yes the corrupt version is a different dungeon. Thanks, Wynn */
        {name: "Sand-Swept Tomb", dbName: "g_Sand-Swept Tomb", canCorrupt: true, dbCorruptName: "g_Corrupted Sand-Swept Tomb"},
        {name: "Ice Barrows", dbName: "g_Ice Barrows", canCorrupt: true, dbCorruptName: "g_Corrupted Ice Barrows"},
        {name: "Undergrowth Ruins", dbName: "g_Undergrowth Ruins", canCorrupt: true, dbCorruptName: "g_Corrupted Undergrowth Ruins"},
        {name: "Galleons's Graveyard", dbName: "g_Galleon's Graveyard", canCorrupt: true, dbCorruptName: "g_Corrupted Galleon's Graveyard"},
        {name: "Fallen Factory", dbName: "g_Fallen Factory"},
        {name: "Eldritch Outlook", dbName: "g_Eldritch Outlook"}
    ]},
    {header: "Raids", data: [
        {name: "Nest of the Grootslangs", dbName: "g_Nest of the Grootslangs"},
        {name: "Nexus of Light", dbName: "g_Orphion's Nexus of Light"},
        {name: "The Canyon Colossus", dbName: "g_The Canyon Colossus"},
        {name: "The Nameless Anomaly", dbName: "g_The Nameless Anomaly"}
    ]},
    {header: "Professions", data: [
        {name: "Fishing", dbName: "c_fishing"},
        {name: "Woodcutting", dbName: "c_woodcutting"},
        {name: "Mining", dbName: "c_mining"},
        {name: "Farming", dbName: "c_farming"},
        {name: "Scribing", dbName: "c_scribing"},
        {name: "Jeweling", dbName: "c_jeweling"},
        {name: "Alchemism", dbName: "c_alchemism"},
        {name: "Cooking", dbName: "c_cooking"},
        {name: "Weaponsmithing", dbName: "c_weaponsmithing"},
        {name: "Tailoring", dbName: "c_tailoring"},
        {name: "Woodworking", dbName: "c_woodworking"},
        {name: "Armouring", dbName: "c_armouring"}
    ]},
    {header: "Misc", data: [
        {name: "Logins", dbName: "c_logins"},
        {name: "Playtime", dbName: "c_playtime"},
        {name: "Total Level", dbName: "g_totalLevel"},
        {name: "Chests Found", dbName: "g_chestsFound"},
        {name: "Quests Completed", dbName: "g_completedQuests"},
        {name: "Discoveries Found", dbName: "c_discoveries"},
        {name: "Mobs Killed", dbName: "g_killedMobs"},
        {name: "Deaths", dbName: "c_deaths"},
        {name: "Wars Won", dbName: "g_wars"}, /* Not fully accurate since multiple players can be in the same one war, but it's good enough. I couldn't find guild war counts in the database */
        {name: "GXP Acquired", dbName: "gu_gxp"}
    ]}
];
const timeButtonData = [
    {name: "Day", time: 86400},
    {name: "Week", time: 604800},
    {name: "Month", time: 2592000},
    {name: "Total", time: -1},
];
const groupButtonData = [
    {name: "ANO"},
    {name: "Guild"},
    {name: "Wynn"}
];

module.exports = {
    EMPTY_LB_TYPE,
    typeButtonData,
    timeButtonData,
    groupButtonData
}

'server-only'
import LeaderboardDisplay from './LeaderboardDisplay.js';
import fetchStats from './fetchStats.js';
import { EMPTY_LB_TYPE, typeButtonData, timeButtonData, groupButtonData} from './buttonData.js';

export const metadata = {
    title: "The Leaderboard",
    description: "A full-fat leaderboard for Wynncraft that rivals the official leaderboard. Brought to you by ANO!"
}

export default async function Page(props) {
    const initialSearchQuery = props.searchParams.name ?? "";
    const initialType = typeButtonData.flatMap(h => h.data).find(d => d.name.toLowerCase() === props.searchParams.type?.toLowerCase()) ?? EMPTY_LB_TYPE;
    const initialCorruptState = props.searchParams.corrupt === "true";
    const initialTime = timeButtonData.find(d => d.name.toLowerCase() === props.searchParams.time?.toLowerCase()) ?? timeButtonData[1];
    const initialGroup = groupButtonData.find(d => d.name.toLowerCase() === props.searchParams.group?.toLowerCase()) ?? groupButtonData[0];

    /* Need to calculate initial results here to prevent a fetch waterfall */
    const initialResults = await fetchStats(props.searchParams.name ?? "ANO", initialType, initialCorruptState, initialTime, initialGroup);
    return (
        <LeaderboardDisplay fetchStats={fetchStats} initialResults={initialResults} initialSearchQuery={initialSearchQuery} initialType={initialType} initialCorruptState={initialCorruptState} initialTime={initialTime} initialGroup={initialGroup}/>
    );
}

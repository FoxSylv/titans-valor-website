'use client';
import { useState, useEffect, useRef } from 'react';
import { useSearchParams } from 'next/navigation';
import Image from 'next/image';
import Link from 'next/link';
import MinecraftSkinLoader from '../../utilities/MinecraftSkinLoader.js';
import getBannerDisplay from '../../utilities/getBannerDisplay.js';
import { EMPTY_LB_TYPE, typeButtonData, timeButtonData, groupButtonData} from './buttonData.js';


let fetchQueue = [];
async function updateResults(fetchStats, searchQuery, selectedTypeButton, isCorrupt, selectedTimestamp, selectedGroup, setIsLoading, setSearchResults, searchResultTableRef) {
    const key = `${searchQuery}.${isCorrupt ? "Corrupt " : ""}${selectedTypeButton.name}.${selectedTimestamp.name}.${selectedGroup.name}`;
    fetchQueue = fetchQueue.concat(key);
    setIsLoading(true);

    const stats = await fetchStats(searchQuery, selectedTypeButton, isCorrupt, selectedTimestamp, selectedGroup);
    if (fetchQueue.at(-1) === key) {
        setIsLoading(false)
        setSearchResults(stats);
        /* Height is set here because scrollTo cannot scroll beyond the previous table height otherwise */
        searchResultTableRef.current.children[0].style.height = "10000rem";
        searchResultTableRef.current.scrollTo({top: 27 * stats.placement, left: 0, behavior: "smooth"});
        searchResultTableRef.current.children[0].style.height = "";
        fetchQueue = [];
    }
}


const selectedButtonClasses = "bg-[#bcbcc2] dark:bg-zinc-600";
const unselectedButtonClasses = "bg-zinc-200 hover:bg-zinc-300 focus:bg-zinc-300 dark:bg-zinc-800 dark:hover:bg-zinc-700 dark:focus:bg-zinc-700 disabled:hover:bg-zinc-200 disabled:focus:bg-zinc-200 disabled:dark:hover:bg-zinc-800 disabled:dark:focus:bg-zinc-800 disabled:cursor-not-allowed";
const corruptButtonClasses = "bg-[#ef4040] dark:bg-red-800";
const allButtonClasses = "shadow-sm rounded-md transition-all duration-300 disabled:opacity-50";
function toButtons(buttonType, buttonData, selectedButton, setSelectedButton, fetchStats, searchQuery, selectedTypeButton, isCorrupt, selectedTimestamp, selectedGroup, setIsLoading, setSearchResults, searchResultTableRef, extraClasses = "") {
    return buttonData.map(b => {
        return (
            <button key={b.name} className={`${allButtonClasses} ${selectedButton.name === b.name ? ((isCorrupt && buttonType === "type") ? corruptButtonClasses : selectedButtonClasses) : unselectedButtonClasses} ${extraClasses} relative`} disabled={buttonType === "type" && isCorrupt && !b.canCorrupt} onClick={async () => {
                    setSelectedButton(b);
                    /* setSelectedButton does not update until the next render, but we need it now. This is ugly, but it works */
                    await updateResults(fetchStats, searchQuery, buttonType === "type" ? (b === selectedTypeButton ? EMPTY_LB_TYPE : b) : selectedTypeButton, isCorrupt, buttonType === "time" ? b : selectedTimestamp, buttonType === "group" ? b : selectedGroup, setIsLoading, setSearchResults, searchResultTableRef)
                }}>
                <p className="my-px">
                    {isCorrupt ? (b.corruptName ?? b.name) : b.name}
                </p>
            </button>
        );
    });
}


const skinBox = `bg-[#8d8265] shadow-[0_0_0_3px_rgb(65,38,36),0_0_0_10px_rgb(139,120,91),0_0_0_15px_rgb(98,78,60),0_0_0_18px_rgb(65,38,36),0_3px_0_18px_rgba(0,0,0,.192)]`;
function getStatsVisual(data, searchResultTableRef, clipboardLink) {
    let statsList = "";
    if ((data.stats?.length ?? 0) !== 0) {
        statsList = 
            <table className="w-full px-8 my-16">
                <tbody>
                    {data.stats.map?.((stat, index) => {
                        return (
                            <tr key={`lb.${stat.left}.${stat.name}.${stat.right}.${index}`} className={index === data.placement ? "font-bold" : ""}>
                                <td className="text-left">
                                    {stat.left}
                                </td>
                                <td className="text-center">
                                    {stat.middle}
                                </td>
                                <td className="text-right">
                                    {stat.right}
                                </td>
                            </tr>
                        );
                    })
                }
            </tbody>
        </table>
    }

    let boxImage = <div className="w-full h-full flex justify-center items-center pb-4">
            <p className="text-9xl font-bold">
                ?
            </p>
        </div>;
    if (data.isGuild) {
        if (data.hasGuildIcon) {
            boxImage = <Image src={`/guild-icons/${data.tag}`} width={200} height={200} alt={`${data.name}'s Guild Icon`} />;
        }
        else if (data.guildBanner.length !== 0) {
            boxImage = <div className="w-full h-full">
                {getBannerDisplay(data.guildBanner, `${data.name}'s guild banner`)}
            </div>;
        }
    }
    else if (data.uuid !== undefined) {
        boxImage = <Image loader={MinecraftSkinLoader} src={data.uuid} width="350" height="350" alt={`${data.name}'s Skin`} />;
    }

    return (<>
        <div className={`${(data.isGuild && !data.hasGuildIcon) ? "w-24" : "w-48"} h-48 m-auto mt-12 overflow-hidden ${skinBox}`}>
            {boxImage}
        </div>
        <p className="text-sm italic text-right mt-6 mb-0">
            {data.isValid ? `Calculated for ${data.isGuild ? "guild" : "user"} ` : (data.error ?? "Error!")}
            <Link href={data.isGuild ? `https://www.wynncraft.com/stats/guild/${data.name}` : `https://www.wynncraft.com/stats/player/${data.uuid}`}>
                {data.name}
            </Link>
        </p>
        <div className="flex gap-1">
            <div className={`w-8 h-8 rounded-full ${((data.online ?? 0) === 0 || !data.isValid) ? "bg-zinc-300 dark:bg-zinc-600" : "bg-lime-400 dark:bg-lime-500"}`}></div>
            <p className="m-0 text-2xl">
                {data.isValid ? (data.isGuild ? `${data.online ?? 0} Members Online` : ((data.online ?? 0) === 0 ? "Not Online" : `Online on WC${data.online}`)) : "Invalid Search Query"}
            </p>
        </div>
        <div className="my-4 w-full h-56 relative overflow-hidden">
            <div className="w-full h-full pr-3 overflow-y-scroll" ref={searchResultTableRef}>
                <div>
                    {statsList}
                </div>
            </div>
            <div className="w-full h-full absolute left-0 top-0 pointer-events-none bg-gradient-to-b from-zinc-100 dark:from-zinc-900 to-20%"></div>
            <div className="w-full h-full absolute left-0 bottom-0 pointer-events-none bg-gradient-to-t from-zinc-100 dark:from-zinc-900 to-20%"></div>
            <div className="absolute right-2 top-0">
                <button className={`${allButtonClasses} ${unselectedButtonClasses} border border-zinc-500 p-1`} onClick={() => navigator.clipboard.writeText(clipboardLink)}>
                    <p role="img" alt="Copy to clipboard" title="Copy To Clipboard" className="m-0">
                        📋
                    </p>
                </button>
            </div>
        </div>
    </>);
};





export default function LeaderboardDisplay(props) {
    let [selectedType, setSelectedType] = useState(props.initialType);
    let [isCorrupt, setIsCorrupt] = useState(props.initialCorruptState);
    let [isTypeSelectorOpen, setIsTypeSelectorOpen] = useState(false);
    let [selectedTypeHeaders, setSelectedTypeHeaders] = useState([]);
    let [selectedTime, setSelectedTime] = useState(props.initialTime);
    let [selectedGroup, setSelectedGroup] = useState(props.initialGroup);
    let [searchQuery, setSearchQuery] = useState(props.initialSearchQuery);
    let [searchResults, setSearchResults] = useState(props.initialResults); /* Prevents a fetch waterfall */
    let [isLoading, setIsLoading] = useState(false);
    const searchbarRef = useRef(null);
    const searchResultTableRef = useRef(null);

    useEffect(() => {
        searchbarRef.current.value = props.initialSearchQuery;
        searchResultTableRef.current.scrollTo({top: 36, left: 0, behavior: "smooth"});
    }, []);


    const typeButtons = typeButtonData.map(group => {
        const buttons = <div className="w-full grid grid-cols-1 xxs:grid-cols-2 gap-3 mt-4 text-base">
                {toButtons("type", group.data, selectedType, button => {
                    setSelectedType((button.name === selectedType.name) ? EMPTY_LB_TYPE : button);
                }, props.fetchStats, searchQuery, selectedType, group.header === "Dungeons" && isCorrupt, selectedTime, selectedGroup, setIsLoading, setSearchResults, searchResultTableRef, "px-2 py-1")}
            </div>

        return (
            <div key={group.header} className="mt-4">
                <button onClick={() => setSelectedTypeHeaders(selectedTypeHeaders.includes(group.header) ? selectedTypeHeaders.toSpliced(selectedTypeHeaders.indexOf(group.header), 1) : selectedTypeHeaders.concat(group.header))}>
                    <h2 className="inline mr-4">
                        {group.header}
                    </h2>
                    <div className={`inline-block w-0 h-0 border-[0.75rem] border-transparent border-l-black dark:border-l-white transition-all motion-reduce:transition-none duration-500 ${selectedTypeHeaders.includes(group.header) ? "rotate-90 -translate-x-1/3 translate-y-1/3" : ""}`}>
                    </div>
                </button>
                <div className={`h-2 mt-1 bg-zinc-200 dark:bg-zinc-800 rounded-sm transition-all motion-reduce:transition-none duration-500 ${selectedTypeHeaders.includes(group.header) ? "!h-0" : ""}`}>
                </div>
                <div className="w-full flex overflow-hidden">
                    <div className={`w-full transition-all motion-reduce:transition-none duration-700 mb-0 max-h-[70rem] ${selectedTypeHeaders.includes(group.header) ? "" : "mb-[-70rem] max-h-0"}`}>
                        {buttons}
                    </div>
                </div>
            </div>
        );
    });
    const timeButtons = toButtons("time", timeButtonData, selectedTime, setSelectedTime, props.fetchStats, searchQuery, selectedType, isCorrupt, selectedTime, selectedGroup, setIsLoading, setSearchResults, searchResultTableRef, "px-3 py-2 xxs:px-4");
    const groupButtons = toButtons("group", groupButtonData, selectedGroup, setSelectedGroup, props.fetchStats, searchQuery, selectedType, isCorrupt, selectedTime, selectedGroup, setIsLoading, setSearchResults, searchResultTableRef, "px-4 py-2");

    
    return (<>
        <div className="mx-4">
            <div className="my-12 pb-20 max-w-md md:max-w-4xl bg-zinc-100 dark:bg-zinc-900 mx-auto rounded-2xl relative">
                <div className="flex justify-center gap-8">
                    <div className={`w-96 px-2 ${isTypeSelectorOpen ? "hidden" : ""} md:block`}>
                        <div className="flex justify-center mt-6 xs:mt-12 mx-6 rounded-xl overflow-hidden">
                            <label htmlFor="lbsearch" className="hidden">Search by username or guild name</label>
                            <input id="lbsearch" type="text" placeholder="Search..." title="Search By Username Or Guild" maxLength="32" className="w-full pl-5 pr-1 py-2 font-minecraft text-2xl bg-zinc-200 dark:bg-zinc-800 z-10 border-r border-white dark:border-zinc-500" onInput={e => setSearchQuery(e.target.value)} ref={searchbarRef}/>
                            <button className={`p-2 ml-[-0.5rem] pl-4 transition-all duration-300 ${unselectedButtonClasses} disabled:cursor-wait`} disabled={isLoading} onClick={async () => await updateResults(props.fetchStats, searchQuery, selectedType, isCorrupt, selectedTime, selectedGroup, setIsLoading, setSearchResults, searchResultTableRef)}>
                                <Image src="/search.svg" alt="Search Button" width="36" height="0" className="invert brightness-50 dark:invert-0" />
                            </button>
                        </div>
                        <p className="h-2 text-xs mt-0.5 ml-16">
                            {searchQuery === "" ? `↳ Defaulting to "ANO"` : ""}
                        </p>
                        <div className="mx-12 mt-2 md:hidden">
                            <button className={`${allButtonClasses} ${unselectedButtonClasses} ${selectedType === EMPTY_LB_TYPE ? "italic" : "font-bold"} w-full px-3 py-2 rounded-xl`} onClick={() => setIsTypeSelectorOpen(true)}>
                                {selectedType.name}
                            </button>
                        </div>
                        <div className="relative">
                            {getStatsVisual(searchResults, searchResultTableRef, `https://titansvalor.org/leaderboard?name=${searchQuery}&type=${selectedType.name}&corrupt=${isCorrupt}&time=${selectedTime.name}&group=${selectedGroup.name}`)}
                            <div className={isLoading ? "absolute top-0 left-0 w-full md:w-96 h-[34rem] md:mx-[-4px] my-[-32px] bg-black/30 dark:bg-white/30 backdrop-blur-sm rounded-lg" : "hidden"}>
                                <div className="w-full h-full flex justify-center items-center">
                                    <div className="w-20 h-20 border-8 border-black dark:border-white border-r-black/20 dark:border-r-white/20 border-b-black/20 dark:border-b-white/20 rounded-full animate-spin">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="flex justify-center gap-1">
                            {timeButtons}
                        </div>
                        <div className="flex justify-center gap-1 mt-2">
                            {groupButtons}
                        </div>
                    </div>
                    <div className={`w-96 mt-4 px-6 md:px-2 ${isTypeSelectorOpen ? "" : "hidden"} md:block`}>
                        <button onClick={() => setIsTypeSelectorOpen(false)} className="md:hidden">
                            <Image src="arrow.svg" width={40} height={40} alt="Back button" className="absolute top-3 left-3 rotate-90 dark:invert duration-200 hover:-translate-x-1 focus:-translate-x-1" />
                        </button>
                        <button className={`${allButtonClasses} ${isCorrupt ? corruptButtonClasses : unselectedButtonClasses} absolute top-6 right-6 md:top-10 md:right-16 w-24`} onClick={async () => {
                            setIsCorrupt(!isCorrupt);
                            await updateResults(props.fetchStats, searchQuery, selectedType, !isCorrupt, selectedTime, selectedGroup, setIsLoading, setSearchResults, searchResultTableRef)
                        }}>
                            <p className="my-1 mx-2">
                                Corrupted
                            </p>
                        </button>
                        {typeButtons}
                    </div>
                </div>
            </div>
        </div>
    </>);
}

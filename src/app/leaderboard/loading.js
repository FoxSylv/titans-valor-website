export default function Loading() {
    return <>
        <h1 className="w-full text-center mt-24">
            Initializing...
        </h1>
    </>;
}

import dynamic from 'next/dynamic'

export const LazyWynnMap = dynamic(() => import('./WynnMap'), { ssr: false })

import { MapContainer, ZoomControl, useMapEvents, ImageOverlay, LayerGroup, Rectangle, Marker, DivIcon, Polyline, Popup } from 'react-leaflet';
import Link from 'next/link';
import L from 'leaflet';
import 'leaflet/dist/leaflet.css';

import Cache from '../../utilities/Cache.js';
import { useState } from 'react';


/* NOTE: Leaflet coords are in (z, x) form. */


const MAP_SIZE = {x: 4608, z: 6144};
const MAP_OFFSET = {x: -2560, z: -6144};
const ROL_SIZE = {x: 1024, z: 1024};
const ROL_OFFSET = {x: -1536, z: -6656};
const EXTRA_BOUNDS = 300;
const DEFAULT_ZOOM = 0;


function ZoomEventHandler(props) {
    const map = useMapEvents({
        zoom() {props.setZoomLevel(map.getZoom())}
    });
}

function getTerritoryCenter(territory) {
    return [0.5 * (territory.location.start[1] + territory.location.end[1]),
            0.5 * (territory.location.start[0] + territory.location.end[0])];
}

const territoryResources = [
    {name: "emeralds", threshold: 9000},
    {name: "ore", threshold: 3600},
    {name: "crops", threshold: 3600},
    {name: "fish", threshold: 3600},
    {name: "wood", threshold: 3600}];
const timeValues = [
    {label: "d", ms: 86400000},
    {label: "h", ms: 3600000},
    {label: "m", ms: 60000},
    {label: "s", ms: 1000}
];
const timeHeldColors = [
    {color: "#55ffff", requiredMs: 1036800000},
    {color: "#55ff55", requiredMs: 432000000},
    {color: "#ffff55", requiredMs: 86400000},
    {color: "#ffaa00", requiredMs: 3600000},
    {color: "#ffaaaa", requiredMs: 0}
];
function getTerritoryVisuals(territories, zoomLevel) {
    return Object.keys(territories).map(territoryName => {
        const territory = territories[territoryName];
        const bounds = [[territory.location.start[1], territory.location.start[0]], [territory.location.end[1], territory.location.end[0]]];

        const textZoomLevel = Math.min(
            33 + Math.floor(9 * zoomLevel), /* Base size */
            Math.min(
                (2 ** (zoomLevel + 0.5 - (territory.guild.prefix.length / 2))) * Math.abs(bounds[0][1] - bounds[1][1]), /* Ensure does not exceed width */
                (2 ** zoomLevel) * (zoomLevel >= 0 ? 0.33 : 0.5) * Math.abs(bounds[0][0] - bounds[1][0]) /* Ensure does not exceed height */
            ));

        let yieldIcons = "";
        const yields = territory.resources;
        if (yields.ore > 0 && yields.crops > 0 && yields.fish > 0 && yields.wood > 0) {
            yieldIcons = `<span style="font-size: 80%">🌈</span>`;
        }
        territoryResources.forEach(resource => {
            for (let left = yields[resource.name]; left >= resource.threshold; left -= resource.threshold) {
                yieldIcons += `<img src="/map/${resource.name}.webp" style="width: 1em">`;
            }
        });
        const popupYields = territoryResources.map(resource => {
            return <li key={`${territoryName}.yields.${resource.name}`} className="m-0">
                {`+${yields[resource.name]} ${resource.name} / hour`}
            </li>;
        });

        const msHeld = Date.now() - new Date(territory.acquired).getTime();
        let timeHeld = "";
        for (let msLeft = msHeld, numTimeValues = 0, currentTimeValue = 0; numTimeValues < 2 && currentTimeValue < timeValues.length; ++currentTimeValue) {
            const timeValueCount = Math.floor(msLeft / timeValues[currentTimeValue].ms);
            if (timeValueCount > 0 || numTimeValues > 0) {
                ++numTimeValues;
                timeHeld += `${timeValueCount}${timeValues[currentTimeValue].label}`;
                msLeft -= timeValueCount * timeValues[currentTimeValue].ms;
            }
        }
        if (timeHeld === "") {
            timeHeld = "0s"
        }
        const timeHeldColor = timeHeldColors.find(c => c.requiredMs <= msHeld).color;


        const territorySize = [(2 ** zoomLevel) * Math.abs(bounds[0][1] - bounds[1][1]), (2 ** zoomLevel) * Math.abs(bounds[0][0] - bounds[1][0])];
        return <Rectangle
                key={`${territoryName}.${territory.guild.prefix}`}
                bounds={bounds}
                fillColor={territory.color}
                color={territory.color}
        >
            <Marker
                position={getTerritoryCenter(territory)}
                icon={new L.divIcon({
                    iconSize: territorySize,
                    html: `<div class="w-full h-full flex justify-center items-center flex-col font-minecraft text-center" style="text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000">
                        <p class="m-0 text-white" style="font-size: ${textZoomLevel}px">
                            ${territory.guild.prefix}
                        </p>
                        ${zoomLevel >= -0.5 ? `<p class="m-0 -mt-3" style="font-size: ${textZoomLevel * 0.6}px">
                            ${yieldIcons}
                        </p>` : ""}
                        ${zoomLevel >= -1 ? `<p class="m-0 -mt-1" style="font-size: ${textZoomLevel / 2}px; color: ${timeHeldColor}">
                            ${timeHeld}
                        </p>` : ""}
                    </div>`
                })}
            >
            <Popup closeButton={false}>
                <h3 className="text-center m-0">
                    {territoryName}
                </h3>
                <h4 className="text-center text-lg m-0 -mt-1">
                    <Link href={`/leaderboard?name=${territory.guild.name}`}>
                        {territory.guild.name}
                    </Link>
                </h4>
                <ul className="m-0 ml-8 mt-2">
                    {popupYields}
                </ul>
            </Popup>
        </Marker>
        </Rectangle>
    });
}
function getTerritoryConnectionVisuals(territories, zoomLevel) {
    return Object.keys(territories).flatMap(territoryName => {
        const territory = territories[territoryName];
        return territory["Trading Routes"].filter(destination => territoryName.localeCompare(territoryName) >= 0 && Object.keys(territories).includes(destination)).map(destination => {
            return <Polyline key={`${territoryName}.${destination}`} color={"#AAAAAA"} weight={2} positions={[
                getTerritoryCenter(territory),
                getTerritoryCenter(territories[destination])
            ]}>
            </Polyline>
        });
    });
}

export default function WynnMap(props) {
    let [zoomLevel, setZoomLevel] = useState(DEFAULT_ZOOM);

    return <>
        {/* I dont understand why the identity transformation flipped the z axis the right way, but whatever */}
        <MapContainer className="absolute top-0 left-0 w-full h-full z-0"
                      crs={L.Util.extend(L.CRS.Simple, {transformation: new L.Transformation(1, 0, 1, 0)})}
                      center={[-2982, 552]}
                      maxBounds={[[MAP_OFFSET.z - ROL_SIZE.z - EXTRA_BOUNDS, MAP_OFFSET.x - EXTRA_BOUNDS], [MAP_SIZE.z + MAP_OFFSET.z + EXTRA_BOUNDS, MAP_SIZE.x + MAP_OFFSET.x + EXTRA_BOUNDS]]}
                      maxBoundsViscosity={0.69}
                      inertiaDeceleration={6969}
                      minZoom={-2.5}
                      zoom={DEFAULT_ZOOM}
                      maxZoom={3.5}
                      zoomSnap={0.5}
                      zoomDelta={0.5}
                      wheelPxPerZoomLevel={120}
                      zoomControl={false}
                      attributionControl={false}
        >
            <ImageOverlay url="/map/rol-base.png" bounds={[[ROL_OFFSET.z, ROL_OFFSET.x], [ROL_SIZE.z + ROL_OFFSET.z, ROL_SIZE.x + ROL_OFFSET.x]]} />
            <ImageOverlay url="/map/map-base.png" bounds={[[MAP_OFFSET.z, MAP_OFFSET.x], [MAP_SIZE.z + MAP_OFFSET.z, MAP_SIZE.x + MAP_OFFSET.x]]} />
            <ZoomControl position="bottomleft" />
            <ZoomEventHandler setZoomLevel={setZoomLevel}/>
            <LayerGroup>
                {getTerritoryVisuals(props.territories, zoomLevel)}
                {getTerritoryConnectionVisuals(props.territories, zoomLevel)}
            </LayerGroup>
        </MapContainer>
    </>;
}

'use client';
import { useState, useEffect } from 'react';
import { LazyWynnMap } from './LazyWynnMap';
import './map.css';

export default function UI(props) {
    let [territories, setTerritories] = useState(props.initialTerritories);
    useEffect(() => {
        const refresh = setInterval(() => {
            props.fetchTerritories().then(t => setTerritories(t));
        }, 10000);
        return () => clearInterval(refresh);
    });


    return <>
        <LazyWynnMap territories={territories} />
        {/* TODO: Add summaries */}
    </>;
}

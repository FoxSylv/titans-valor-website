'server-only';
import fetchTerritories from './fetchTerritories.js';
import UI from './UI.js';

export default async function Page() {
    const initialTerritories = await fetchTerritories();

    return <UI initialTerritories={initialTerritories} fetchTerritories={fetchTerritories} />;
}

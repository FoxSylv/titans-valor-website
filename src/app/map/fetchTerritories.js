'use server';
import Cache from '../../utilities/Cache.js';
import dbQuery from '../../data/dbQuery.js';
import extraTerritoryData from './extraTerritoryData.json';

const historyCache = new Cache(60000);
const currentCache = new Cache(10000);

let wynntilsDataCache = new Cache();
async function getGuildColor(guildName) {
    const endpoint = "https://athena.wynntils.com/cache/get/guildListWithColors";
    const guildColors = await wynntilsDataCache.get(endpoint, async () => await fetch(endpoint, {cache: "no-store"}).then(response => response.json())).catch(e => wynntilsDataCache = new Cache());
    const override = guildColors?.[Object.keys(guildColors).find(guild => guildColors[guild]._id === guildName)]?.color;
    if ((override ?? "") !== "") {
        return override;
    }

    /* Copied from the old WynnData map (https://www.wynndata.tk/map/). Fallback if wynntils dies */
    let hash = 0;
    for (let i = 0; i < guildName.length; i++) {
        hash = guildName.charCodeAt(i) + ((hash << 5) - hash);
    }
    let color = '#';
    for (let i = 0; i < 3; i++) {
        let value = (hash >> (i * 8)) & 0xff;
        color += ('00' + value.toString(16)).substr(-2);
    }
    return color
}

export default async function fetchTerritories(timestamp) {
    const endpoint = "https://api.wynncraft.com/v3/guild/list/territory";
    let territories = await currentCache.get(endpoint, async () => await fetch(endpoint, {cache: "no-store"}).then(response => response.json()));

    if (timestamp) {
        /* Pre-load past week of data */
        const historyQuery = `
            SELECT *
            FROM terr_exchange
            WHERE time > ${60 * Math.floor(timestamp / 60000) - 604800}
        `;
        let territoryHistory = historyCache.get(historyQuery, async () => await dbQuery(historyQuery));

        /* TODO: extrapolate backwards */
    }

    /* Extra data */
    for await (const territory of Object.keys(territories)) {
        /* Add extra data from non-Wynn API sources */
        const color = await getGuildColor(territories[territory].guild.name);
        territories[territory].color = `#${"0".repeat(7 - color.length)}${color.slice(1)}`; /* Edge case where the color code starts with 0 in Wynntils API */
        Object.keys(extraTerritoryData[territory] ?? {}).forEach(key => territories[territory][key] = extraTerritoryData[territory][key]);
        
        /* Fallbacks if Wynn API dies in a weird way */
        territories[territory] ??= {};
        territories[territory].location ??= {}
        territories[territory].location.start ??= [-10000, -10000];
        territories[territory].location.end ??= [-10000, -10000];
        territories[territory].acquired ??= 0;
        territories[territory].guild ??= {};
        territories[territory].guild.uuid ??= "0";
        territories[territory].guild.prefix ??= "ERROR";
        territories[territory].guild.name ??= "WYNN API ERROR";
        territories[territory].resources ??= {};
        territories[territory].resources.ore ??= 0;
        territories[territory].resources.crops ??= 0;
        territories[territory].resources.fish ??= 0;
        territories[territory].resources.wood ??= 0;
        territories[territory].resources.emeralds ??= 0;
        territories[territory]["Trading Routes"] ??= [];
    }

    return territories;
}

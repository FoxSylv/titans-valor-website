'use client'

export default function Error({ error, reset }) {
    return (
        <div className="mt-48 text-center">
            <h1>
                404
            </h1>
            <h2>
                Page Not Found
            </h2>
        </div>
    );
}

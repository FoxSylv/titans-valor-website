export default function Indented(props) {
    return (
        <div className="ml-4 md:ml-12">
            {props.children}
        </div>
    );
}

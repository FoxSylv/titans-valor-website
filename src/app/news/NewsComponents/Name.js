import Link from 'next/link';

/* The override attribute determines the leaderboard username, in case of nicknames */
export default function Name(props) {
    let name = props.override ?? props.children;

    const base = <strong>{props.children}</strong>;
    if (props.nouser) {
        return base;
    }

    return (
        <Link href={`https://titansvalor.org/leaderboard?name=${name}`} className="no-link-styling">
            {base}
        </Link>
    );
}

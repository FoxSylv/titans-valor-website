export function QnA(props) {
    return (
        <div>
            {props.children}
        </div>
    );
}
export function Q(props) {
    return (
        <h4>
            {props.children}
        </h4>
    );
}
export function A(props) {
    return (
        <p className="ml-4 md:ml-12">
            {props.children}
        </p>
    );
}

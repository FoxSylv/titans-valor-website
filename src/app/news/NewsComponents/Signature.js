export default function Signature(props) {
    return (
        <p className="text-right font-bold">
            {props.children}
        </p>
    );
}

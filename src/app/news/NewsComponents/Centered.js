export default function Centered(props) {
    return <div className="m-auto flex flex-col justify-center items-center text-center">
        {props.children}
    </div>;
}

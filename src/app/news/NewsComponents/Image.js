import NextJSImage from 'next/image';

export default function Image(props) {
    return (
        <NextJSImage width={0} height={0} sizes={"66vw"} className="w-2/3 h-auto" style={{width: `${props.width}rem`}} {...props} />
    );
}

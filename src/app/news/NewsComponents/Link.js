import NextJSLink from 'next/link';

export default function Link(props) {
    return (
        <NextJSLink rel="noopener noreferrer" target="_blank" {...props}>
            {props.children}
        </NextJSLink>
    );
}

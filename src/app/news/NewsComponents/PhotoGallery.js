import './PhotoGallery.css';

export default function PhotoGallery(props) {
    return (
        <div className="PhotoGallery-container">
            {props.children}
        </div>
    );
}

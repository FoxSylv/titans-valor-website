import NextJSImage from 'next/image';

/* Some images are far taller than wider, making them look goofy if they take up a "normal" amount of width */
export default function TallImage(props) {
    return (
        <NextJSImage width={0} height={0} sizes={"33vw"} className="w-1/3 min-w-80 h-auto" style={{width: `${props.width}rem`}} {...props} />
    );
}

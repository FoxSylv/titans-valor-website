import Name from './Name';

export default function Names(props) {
    /* A list is grammatically formatted with no commas if it contains two elements, requiring some modified logic here to handle that case */
    const delimiter = props.children.includes(",") ? ',' : " and";

    let names = props.children.split(delimiter).map(string => {
        const name = string.split("and ").at(-1).trim();
        return <Name key={`Names.${name}.${props.overrides?.[name] ?? name}`} override={props.overrides?.[name] ?? name}>
                {name}
            </Name>;
    });

    /* More special cases for only 2 items required */
    return names.flatMap((name, nameNum) => {
        if (nameNum === names.length - 1) {
            return props.partial ? [name, ", "] : [" and ", name];
        }
        else {
            return [name, names.length === 2 ? "" : ", "]
        }
    });
}

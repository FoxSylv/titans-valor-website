import * as TT from '../NewsComponents';

export default function Page() {
    return (<>
        <h2>
            Recent Happenings 📅
        </h2>
        <p>
            A ton has happened in the 8 months that have gone by since the last volume, so this segment is going to be a bit bigger than usual. I do also want to limit this, so not everything can be included here.
        </p>
        <p>
            First of all, our guild housing island has now surpassed 5000 upvotes, and by the looks of it, we are on the horizon to 6000!
        </p>
        <blockquote>
            <p>
                editor's note: we hit 6000+ upvotes now.
            </p>
        </blockquote>
        <TT.Centered>
            <TT.Image src="/news/vol12/6k.png" alt="6000 upvotes" />
            <p>
                I am not late for this screenshot at all. Bib sent the updated screenshot
            </p>
        </TT.Centered>
        <p>
            Congrats to <TT.Name>
                Calluum
            </TT.Name> for hitting the two-year mark as our leader! Your dedication, creativity, and just being an amazing person have shaped Titans Valor into the fantastic guild it is today. Here’s to all the epic events, experiences and adventures we’ve had under your leadership.
        </p>
        <p>
            Titans Valor has now also reached level 103! We also held a raffle during a GXP grind event in November, congrats to <TT.Names>
                Lamdakis, HeyZeer0, Getbuttertewurst, Kebabdoner, EchoLuu, BlueTheSniper, Synthetic_Toes, Cloudedy, and Hallomifrendo
            </TT.Names> on winning that. <TT.Name>
                OwORawr
            </TT.Name> also majorly contributed 140 billion XP to the guild so a massive shoutout for that as well!
        </p>
        <p>
            Many festivals have also gone by, and a new rank, Warlock, was introduced as a way to ease the transition between Artificer and Sorcerer.
        </p>
        <p>
            To round out the recent events, let’s talk about the promotions! I am not going to be covering Sorcerer and Warlock promotions as there would simply be way too many to cover. But, help me give congratulations to <TT.Names>
                Kloweechu, Cloudedy, Anons, and Teishoko
            </TT.Names> on Artificer and warmest congratulations to <TT.Name>
                CrispperPlanet
            </TT.Name> on Titan.
        </p>
        <h3>
            What’s in the Cupboard?: ANO Cabinet Review
        </h3>
        <h4>
            Spirits
        </h4>
        <p>
            Our Adonis alliance has been bustling with a plethora of engaging events lately! From ESI's lively "Guess The OST'' challenge to our thrilling "Capture the Flag" showdown, and Avicia's exciting game of "Hide and Seek," the fun never stops.
        </p>
        <p>
            We also enjoyed a festive Secret Santa event and a Death Run, hosted by us. Kicking off the new year with a bang, ESI treated us to a scavenger hunt. And most recently, Avicia hosted a unique new event, "Adoniscrafting", where participants attempted to reverse engineer crafted items. With recipes ranging in difficulty, the race was on to decipher them before others did!
        </p>
        <p>
            Additionally, our guild housing projects are progressing steadily, with the majority of the Castle’s exterior complete, and the interior furnishing showing impressive progress.
        </p>
        <TT.Centered>
            <TT.Image src="/news/vol12/housing1.png" alt="Castle exterior" />
            <p>
                Had to put my FOV on Quake Pro for this one. It’s amazing!
            </p>
            <TT.Image src="/news/vol12/housing2.png" alt="Castle interior" />
            <p>
                The interiors are looking a lot better than those plain walls from before.
            </p>
            <TT.Image src="/news/vol12/housing3.png" alt="Castle interior" />
            <p>
                Furnishings add that extra touch to make it all the more better. Thanks GamingProf!
            </p>
        </TT.Centered>
        <TT.Indented>
            <p>
                If you're intrigued by these events or eager to collaborate on future ones, why not apply to join the Spirits cabinet? All current members are eligible, and involvement in a cabinet boosts your chances of promotion down the line. Let your imagination soar, there are no limits to what you can create!
            </p>
        </TT.Indented>
        <h4>
            Brilliance
        </h4>
        <p>
            Titans Brilliance, our hub of coding and creative endeavors, has been buzzing with activity during the eight-month hiatus of the Titan Times. Exciting updates are underway. Starting with the revamp of the Titan Times website by <TT.Name>
                FoxSylv
            </TT.Name>. With mobile support in the works, accessing the Titan Times will be more convenient than ever.
        </p>
        <p>
            Meanwhile, our Discord bot, Valor, is undergoing a complete rewrite aimed at simplifying setup, improving performance, and enhancing integration with the Wynncraft API v3.
        </p>
        <TT.Indented>
            <p>
                As always, we're on the lookout for fresh talent to join our ranks. If you possess coding or creative skills, don't hesitate to apply! Applications are open to ALL current members, and active participation in the cabinet can pave the way for future promotions!
            </p>
        </TT.Indented>
        <h3>
            Military Updates ⚔️
        </h3>
        <blockquote>
            <p>
                editor's note: Season 16 just ended. Season 17 will begin soon.
            </p>
        </blockquote>
        <TT.Centered>
            <TT.Image src="/news/vol12/rewards.png" alt="Guild SR leaderboard" />
            <p>
                Asoart Jumpscare
            </p>
        </TT.Centered>
        <TT.Indented>
            <p>
                A quick note for Ocean Trials participants: Anode, the mod we use to track wars, <strong>is no longer required</strong>. The mod is obsolete and will no longer be maintained due to Wynn API changes. We can finally reliably track the wars you do. The rules for tracking rules are as follows:
            </p>
        </TT.Indented>
        <ul>
            <li> You must be online for ~20 minutes after completing your war. The duration changes with the number of players online </li>
            <li> Wynn API is available </li>
            <li> Wynn API does not report any funny errors for your uuid / username. </li>
        </ul>
        <p>
            In the realm of Guild Wars, five seasons have breezed by, witnessing tens of thousands of wars that have been fought valiantly. A heartfelt thank you to all contributors, with special nods to <TT.Names partial>
                Kebabdoner, CrispperPlanet, EchoLuu</TT.Names>, and <TT.Name override="kloweechu">Klowee
            </TT.Name>, who dominated in wars during Seasons 12 to 15, respectively. Also, many congratulations to <TT.Name>
                Kebabdoner
            </TT.Name> for being promoted to Chief! They’ve passed 3000 wars and it doesn’t seem like they’ll be stopping any time soon!
        </p>
        <p>
            As we dive into Season 16, our hold on the ocean is robust, with Titans Valor asserting control over vast expanses. The seas may churn, but our resilience stands unyielding!
        </p>
        <h2>
            Main Stage: Mastering the Art of Professions 👨‍🍳
        </h2>
        <blockquote>
            Written by C0rrupted
        </blockquote>
        <p>
            Welcome, adventurers, to the realm of professions in Wynncraft, where dedication and persistence are the keys to mastering your craft. While Wynncraft is not as grind-intensive as some other games, delving into professions here demands a considerable investment of time and effort.
        </p>
        <h4>
            The Professions Grind
        </h4>
        <p>
            Each profession boasts a maximum level cap of 132. As of version 2.0, reaching this pinnacle requires an astonishing 642,697,533 XP, so prepare to embark on a journey of dedication, perseverance, and extreme boredom. Many adventurers find the grind becomes notably monotonous and arduous beyond level 80, but fear not! Gear up with Gathering XP (GXP) equipment and guild boosts to lighten the stress as you trek toward level 100 and beyond. If you can, try to join a world that has a Profession XP or Speed bomb, however, due to the demand, these worlds are often full.
        </p>
        <p>
            Professions can be a lucrative endeavor, with players raking in profits by selling Tier 3 Dernic materials, high-quality crafted gear, and even the scrap from discarded crafted items. While it's rare to turn a profit from selling materials for levels 80 and under, certain ingredients, especially rare ones or those sold in bulk, can fetch handsome sums.
        </p>
        <p>
            If you're considering delving into professions on your first playthrough, though, think twice. The time investment required to master professions far exceeds that needed for combat leveling and loot runs combined. However, if you seek an alternative source of income, professions offer a viable pathway, albeit with lower profits over the time you invest in it.
        </p>
        <h4>
            Tools of the Trade
        </h4>
        <p>
            As soon as you start a new character, you will be able to use the tier 1 tools. Once you reach level 5, you will be able to use tier 2 gathering tools. Following that, you will be able to use a new tier of tools every 10 levels. The fastest tools are tier 12, unlocked at level 105. Each gathering profession requires its own tool, with woodcutting requiring an axe, mining requiring a pickaxe, farming requiring a scythe, and fishing requiring a fishing rod. Alongside tools, you will be able to gather a new resource every 10th level, leading to higher XP gains.
        </p>
        <h4>
            Crafting Wonders
        </h4>
        <p>
            Armouring, Weaponsmithing, Tailoring, Woodworking, Jeweling, Cooking, Scribing, and Alchemism are the eight crafting professions. Each crafting skill unlocks new possibilities with every level gained, granting access to higher-tier recipes. Each discipline offers a unique array of items waiting to be brought to life through your craftsmanship.
        </p>
        <p>
            Armouring allows you to fashion helmets and chestplates from ingots and paper, while Weaponsmithing lets you forge deadly daggers and spears using wood and ingots. Tailoring unlocks the art of crafting leggings and boots from string and ingots, and Woodworking offers the opportunity to create bows, wands, and reliks from wood, string and oils.
        </p>
        <p>
            Jeweling grants you the ability to craft exquisite accessories from gems and oil, while Cooking invites you to concoct dishes from grains and meat. Scribing empowers you to craft powerful scrolls using paper and oil, and Alchemism enables the creation of potent potions from oil and grain. These dishes, potions and scrolls can provide you with buffs and upgrade your stats, to aid you in battle.
        </p>
        <TT.Centered>
            <TT.Image src="/news/vol12/profs.png" alt="Professions diagram" />
            <p>
                This diagram shows the link between each of the crafting and gathering professions. Credits to Libraeum!
            </p>
        </TT.Centered>
        <h4>
            Spicy Ingredients
        </h4>
        <p>
            Ingredients play a crucial role in the crafting process in Wynncraft, adding depth and complexity to the creation of items. With over 500 unique ingredients available, each with its own properties and effects, mastering the art of ingredient selection is key to crafting success.
        </p>
        <p>
            When crafting an item, up to six ingredients can be used, providing a multiplier effect based on their level and tier. These ingredients can enhance the features of the crafted item, such as increasing main attack damage, giving skill points or providing mana regen. Regardless of the level or tier of the ingredient, using all six slots will yield a minimum of 10 times more experience than crafting without ingredients, making them invaluable for leveling up crafting skills efficiently.
        </p>
        <p>
            Ingredients can be obtained through various means, from killing mobs to doing quests. Some ingredients are common and easily accessible, while others are rare and require careful exploration or trading to acquire. Experimenting with different combinations is part of the fun and challenge of crafting in Wynncraft.
        </p>
        <p>
            To maximize the benefits, crafters often strategize and plan their ingredient selection based on the desired outcome of the crafted item. Whether aiming for maximum spell damage or minimum skill point cost, the right combination of ingredients can change whether an item is good or excellent.
        </p>
        <h4>
            Conclusion
        </h4>
        <p>
            Wynncraft’s professions offer a wide variety of gameplay experiences, from the thrill of gathering rare materials to the satisfaction of crafting amazing items. By harnessing the power of ingredients, crafters can unlock the full potential of their skills. So what are you waiting for? The journey awaits!
        </p>
        <h3>
            Member Shout of the Publication
        </h3>
        <p>
            <TT.Name>
                OwORawr
            </TT.Name>, for all that Guild XP grinding! (140 Billion!)
        </p>
        <p>
            <strong>
                Trivia Fact
            </strong>
        </p>
        <TT.TriviaFact>
            Honey never spoils. Archaeologists have found pots of honey in ancient Egyptian tombs that are over 3,000 years old and still perfectly edible
        </TT.TriviaFact>
        <h3>
            Closing Remarks
        </h3>
        <p>
            Writing this edition of the Titan Times has been an absolute blast! I've thoroughly enjoyed writing it and I hope you enjoyed reading it too. A massive thank you to my fellow Brilliance Cabinet members for their invaluable feedback and support in reviewing my drafts, making this journey even more enjoyable. Stay tuned for the next issue, where we'll have more new and exciting updates ready for you. Until then, happy adventuring, Titans!
        </p>
        <TT.Signature>
            -C0rrupted
        </TT.Signature>
    </>);
}

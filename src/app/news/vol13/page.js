import * as TT from '../NewsComponents';

export default function Page() {
    return (<>
        <h2>
            Recent Happenings 📅
        </h2>
        <p>
            Exciting news, adventurers! The Farplane, Titans Valor, and Avicia have teamed up to bring you the Wynncloud Modded Server! Featuring over 180 mods, this custom-made modpack offers a diverse range of experiences, from tech and magic to mining and adventuring. ANO members will have priority access, with the IP being shared exclusively with those who react to the announcement on our discord. While there's no player limit, we've implemented a queue system to ensure a smooth experience for everyone. More details in the events channel on our discord! We can't wait to embark on this new adventure with you all!
        </p>
        <p>
            It is with a heavy heart that we bid farewell to one of our esteemed council members, EchoLuu, after an incredible 17 months within our guild. From the moment she joined, Echo has left an indelible mark on our community. As a cherished friend, dedicated council member, and formidable force in guild wars, Echo's contributions have been invaluable. She embraced roles with grace, welcomed new members with open arms, and offered assistance whenever.
        </p>
        <p>
            While personal challenges and a desire for new adventures beckon her beyond our halls, Echo leaves behind a legacy that will be cherished. Our bond remains strong, and we encourage you to reach out and wish her well in her endeavors. Farewell Echo, and may your path be filled with happiness and success.
        </p>
        <p>
            It's time to shine a spotlight on our dedicated members and celebrate their outstanding achievements! Join us in congratulating <TT.Names>
                Syetriz, Mehulrocks, evets_a, CattenXP, Twah2em, JordyWasTaken, 123notded, JuanTheShoe, Zepart, Melkypoos, WanPunch, CinnaBunzx, mentofh, suzukzmiter, cornyduck, KillAuraRocks, seikyi, Itsviper01, LososCroissant, plump_hippogriff, and Fudgeboard
            </TT.Names> on their promotion to Sorcerer. Congrats to <TT.Names>
                eepygirlfailure, Jurrior, thatlamppostguy, Bolitifed, C00D, Zepart, and Cap_Jigglebeard
            </TT.Names> for their promotion to Warlock. Huge congratulations to <TT.Names>
                C0rupted, OwORawr, admiral_kiwi, and EyesYetOpen
            </TT.Names> on Artificer!
        </p>
        <p>
            A massive round of applause goes out to <TT.Name>
                kloweechu
            </TT.Name> for reaching the Titan rank! Her exceptional dedication to warring, is evident through remarkable achievements such as getting Champion of the Ocean twice in a row. Not only has she demonstrated her prowess on the battlefield, but she’s also been an active and valued member of our community as a whole. We're thrilled to see all of these people rise to new heights within our community and look forward to the positive impact they'll continue to make. Here's to many more successes on their journey with us!
        </p>
        <h3>
            What’s in the Cupboard?: ANO Cabinet Review
        </h3>
        <h4>
            Spirits
        </h4>
        <p>
            Warriors, we're thrilled to announce the successful conclusion of our Flying Kick Tournament! Let's take a moment to celebrate the remarkable feats of skill and agility displayed by our valiant competitors.
        </p>
        <p>
            The tournament bore witness to an exhilarating spectacle as warriors, armed with a specialized Morph build augmented by Gale's Sight, clashed in electrifying aerial battles. Participants, standing on opposite ends of the platform, had to showcase their prowess with charge and uppercut, aiming not to kill, but knock their opponent off the platform. These adrenaline-fueled clashes, marked by strategic finesse and lightning-fast reflexes, illuminated the arena with a dazzling display of combat prowess.
        </p>
        <p>
            Congratulations to our tournament champions! The winner, MrRickroll, demonstrated unparalleled skill and determination, walking away with a grand prize of 2 stacks of LE. Our formidable runner-up, C00D, showcased remarkable prowess, earning a well-deserved 1 stack of LE. Let's also extend our applause to the third and fourth place contenders, linnyflower and Ugastag, who exhibited outstanding resilience, each securing 32 LE. A round of applause to all participants for their spirited efforts!
        </p>
        <p>
            We're very grateful to all the warriors who participated in the tournament, contributing to its success and making it a memorable event for everyone involved.
        </p>
        <p>
            As we reflect on the excitement of the Flying Kick Tournament, let's look forward to more thrilling events and challenges ahead. Stay tuned for future announcements and opportunities to showcase your skills in upcoming events.
        </p>
        <TT.PhotoGallery>
            <TT.Image src="/news/vol13/flyingkick1.png" alt="Tournament" />
            <TT.Image src="/news/vol13/flyingkick2.png" alt="Tournament" />
            <TT.Image src="/news/vol13/flyingkick3.png" alt="Tournament" />
            <TT.Image src="/news/vol13/flyingkick4.png" alt="Tournament" />
        </TT.PhotoGallery>
        <TT.Indented>
            <p>
                If you're intrigued by these events or eager to collaborate on future ones, why not apply to join the Spirits cabinet? All current members are eligible, and involvement in a cabinet boosts your chances of promotion down the line. Let your imagination soar, there are no limits to what you can create!
            </p>
        </TT.Indented>
        <h4>
            Brilliance
        </h4>
        <p>
            Welcome back to the Brilliance cabinet, where creativity knows no bounds! In this volume, we're thrilled to showcase the ingenuity and talent of our guild members as they continue to push the boundaries of what's possible in Wynncraft.
        </p>
        <p>
            In other news, our Discord bot, Valor, has recently got a new command. The guild overview command (-guild) shows a basic overview of a guild, which can show online members, details about the guild, war counts, XP contributions and more.
        </p>
        <blockquote>
            <p>
                This command was created by C0rrupted.
            </p>
        </blockquote>
        <TT.Centered>
            <TT.TallImage src="/news/vol13/brilliancecab.png" alt="New command" />
            <p>
                Example -guild command response
            </p>
        </TT.Centered>
        <TT.Indented>
            <p>
                As always, we're on the lookout for fresh talent to join our ranks and contribute to our ever-growing creative endeavors. Whether you're a seasoned coder, a master builder, or simply brimming with innovative ideas, there's a place for you in Titans Brilliance. Don't hesitate to reach out and get involved – together, we can continue to push the boundaries of creativity in Wynncraft!
            </p>
        </TT.Indented>
        <h3>
            Military Updates ⚔️
        </h3>
        <p>
            In the middle of Season 17, our guild is buzzing with excitement and activity! Last season, <TT.Name>
                kloweechu
            </TT.Name> snagged the title of Champion of the Ocean once again, showing us all how it's done. We've got a brand-new addition to our FFA lineup this season: the Jungle FFA! As we hit the halfway mark of the season, let's keep the momentum going strong. Together, we'll conquer every challenge Season 17 throws our way!
        </p>
        <TT.Centered>
            <TT.Image src="/news/vol13/jungleffa.png" alt="New FFA territories" />
            <p>
                Our new Jungle FFA!
            </p>
        </TT.Centered>
        <h2>
            Main Stage: How to Fix Professions
        </h2>
        <blockquote>
            Written by FoxSylv
        </blockquote>
        <p>
            The main stage for the last issue discussed the basics of professions and how they worked. For this main stage, I’m going to dive into what I view the problems are with the current implementation of professions, and what could be done to fix them. Even if you don’t agree with my critiques or solutions, I hope this analysis sparks a discussion to help improve the profession system, which is in dire need of work.
        </p>
        <h4>
            The Gameplay is fine
        </h4>
        <p>
            I personally believe that at its core, the crafting system in Wynncraft is amazing. It provides an entire new avenue for players to uniquely min/max their loadouts, whilst also providing multiple new paths for players to enjoy the game. The unfathomable potential the ingredients system provides makes it super fun to craft a few items. It’s clear the content team put a lot of thought into ingredients and crafting.
        </p>
        <p>
            To get a feel for what I’m saying, I would highly recommend starting an Ironman Craftsman profile. For the first 20 or so levels, it’s spectacularly fun trying out different combinations of ingredients and just seeing what you can make. While more laid-back than most other content, gathering and crafting at its core, is fun.
        </p>
        <p>
            This enjoyment is not a one-off phenomenon, either. A lot of new players I see on twitch streams actually really enjoy professions. To them, professions are a calm break between the frenetic combat activities. That mindset fades as the grinding slog intensifies, however. What this all points to is that the problem with professions is not a gameplay issue, but rather a lack of support from the rest of the Wynncraft experience.
        </p>
        <h4>
            The Leveling Problem
        </h4>
        <p>
            The first major problem that’s been identified here is that leveling gets monotonous and boring. There needs to be another gameplay system in place to enhance the process of leveling up professions. Currently, the closest Wynncraft gets towards this goal is with mini-quests. In effect, however, mini-quests just reduce the experience required to level up slightly rather than provide any actual meaningful enhancement to the underlying gameplay.
        </p>
        <p>
            My suggestion has a few stages. The first step is to integrate professions into the Silverbull Trading Company. In-universe, the Company is meant to be a ”a larger conglomerate in the world of Wynncraft that deals with various services” (2.0.3 Patch Notes), so having various profession ”services” would be a perfect fit. Adding a Silverbull building to Detlas and reworking the building in Cinfras would help integrate professions much more directly into the world (the current Silverbull building in Cinfras is beautiful, but it unfortunately looks placed onto the world rather than being from it).
        </p>
        <p>
            Next, in both of these buildings, offer profession quests. Allow users to choose one of a number of quests in a profession skill of their choice, with the number of options and possible difficulties scaling with their level in that skill. Importantly, the quests should be randomly generated after each completion (or fail) and should not be permanently stagnant. Here’s some quests that I threw together as an example of what they may look like:
        </p>
        <TT.PhotoGallery>
            <TT.Image src="/news/vol13/quest1.png" alt="Example quest" />
            <TT.Image src="/news/vol13/quest2.png" alt="Example quest" />
            <TT.Image src="/news/vol13/quest3.png" alt="Example quest" />
            <TT.Image src="/news/vol13/quest4.png" alt="Example quest" />
            <TT.Image src="/news/vol13/quest5.png" alt="Example quest" />
        </TT.PhotoGallery>
        <p>
            While the specifics of these quests should be left up to the devs and QA team, their main objective is not only to spice up the levelling process, but also introduce new players to ingredients, how they interact, and how to build items they might want. The quests would also shift the tedious grind required to level up into fun brain teaser challenges, while still acting as a break from other more frenetic activities like professions are now.
        </p>
        <p>
            To complement the new quest system, adding a dedicated ingredient trading hub similar to the Bazaar in hypixel skyblock would be massively beneficial. Ingredients are all identical to each other, and users frequently gather lots of many different types while out adventuring. With the extremely limited trade slots in the trade market, most ingredients get sold straight from the ingredient pouch, which I feel is a massive waste of potential.
        </p>
        <p>
            A great QOL addition in the bazaar would be a button to auto-sell everything in the ingredient pouch. I believe that there still should be an option to sell directly from the ingredient pouch, since it is a good convenience mechanic that can also be used to establish price floors.
        </p>
        <h4>
            The Low-Level Problem
        </h4>
        <p>
            Even if leveling professions was improved this way, there would still be no reason for new characters to do professions. It is faster and easier to just level combat to get better gear, so the optimal way for players to progress would still be to ignore professions entirely until the endgame. I feel this is another problem that needs to be tackled for a comprehensive professions rework.
        </p>
        <p>
            My solution would be to integrate professions into the ability tree. Specifically, have each archetype be a specialist of one crafting skill, with their adjacent archetypes having one gathering skill in common. Here’s an example of what that may look like overlaid onto the mage ability tree:
        </p>
        <TT.Centered>
            <TT.TallImage src="/news/vol13/tree.png" alt="Tricoloured ability tree" />
        </TT.Centered>
        <p>
            For all archetype-aligned abilities, associate them with their archetype’s specialty (e.g. Ophanim and Alchemism). For unaligned abilities, associate them with the gathering profession the archetypes’ specialties next to them have in common (e.g. Cheaper Heal would have Alchemism and Tailoring as neighbors, so it would be associated with Farming; Cheaper Ice Snake would have only Tailoring as a neighbor, so it could be associated with Mining). Some abilities could also be unassociated with a profession (e.g. Purification may be unassociated since it’s completely centered). The specifics of abilities’ alignments should be left to the dev and QA team, but this general rule of thumb can serve as a suggestion and starting point.
        </p>
        <p>
            Then, once these associations have been established, the abilities scale in power relative to the level of its associated profession skill’s level. Critically, I don’t believe that there should be any hard minimum level requirements so that players can try out different playstyles before investing any appreciable time into them. For an example of what this may look like, consider the mage ability Fluid Healing, both as it is right now and how it could scale linearly with Alchemy level:
        </p>
        <TT.PhotoGallery>
            <TT.Image src="/news/vol13/ability1.png" alt="Original ability" />
            <TT.Image src="/news/vol13/ability2.png" alt="Modified ability" />
        </TT.PhotoGallery>
        <p>
            Despite the small change, the impact would be multifold. Players would have the additional choice of leveling up their professions to become stronger through their abilities rather than a linear trajectory focusing exclusively on their combat level. Not only that, but this archetypebased specialization would narrow the 12 professions to work on (which may be daunting and/or overwhelming) down to just a single crafting skill and its requisite gathering skills. Plus, the specialization would foster player trading of different crafted items to acquire items outside of their one crafting skill.
        </p>
        <p>
            With proper scaling, abilities higher in the tree could remain useful later in the game as they would grow in power with the player rather than being balanced exclusively for early-game players. The current system has some abilities which are useful more for the archetype point rather than as an actual buff (e.g. Wand Proficiency II and Wisdom in the mage ability tree), which I feel is wasted potential. Plus, this scaling idea would give the devs another way to balance the ability tree and archetypes.
        </p>
        <p>
            As a further benefit, new characters will get to Detlas at around the same time that they need to begin their archetype-focused crafting journey. Placing a Silverbull building in Detlas would give immediate access to the profession quests, and a potential mainline Aledar and Tasim tutorial quest could be added to explain to new players how professions integrate with their character’s profile. The specifics of everything would of course be left up to the dev and QA teams, but I believe this addition of professions to the ability tree would heighten the overall gameplay experience greatly.
        </p>
        <h4>
            Durability
        </h4>
        <p>
            As a final point to look at it, the durability system in Wynncraft is terrible. The objective of durability systems in most games is to establish a limit for goods to be used before they are inevitably destroyed. Without that inevitable destruction, Wynncraft instead implements a fragility system: ”durability” is just a bar to be tracked to prevent annoyance.
        </p>
        <p>
            At best, durability in Wynncraft is a timer for when you are forced to withdraw from your other gameplay. At worst, the durability system is cause for an especially aggravating failure. The determination of max durability is made with the assumption of repairs, which compounds this issue further. Most crafted goods have a durability below 200, requiring frequent trips to the Blacksmith.
        </p>
        <p>
            A simple solution would be to implement the inevitable destruction of items to reject the current fragility system. In other words, I would recommend removing the repairing and scrap mechanics entirely. To compensate, the starting durability of items should be greatly increased so that crafted tools remain viable in ordinary play. A number I am envisioning as a suggestion would be to just multiply all crafted items’ durability by 100.
        </p>
        <p>
            I really like how in the current system, crafted goods lose their effectiveness as their durability gets low, and I believe this mechanic should be preserved. Should the repairing system be removed, the slow dropping of stats would also act as a soft warning that a new set of crafted gear is needed before it shatters fully. I would, however, suggest moving the durability barrier for when this warning occurs to start at a much lower durability value. For example, 20% of the item’s maximum durability for this effect would be my personal recommendation.
        </p>
        <p>
            Making gathering tools have an inevitable destruction as well would be an amazing way to get people to do dungeons more. As it currently stands, dungeons are wildly underappreciated and underutilized, as there is no reason to play them repeatedly. Since dungeons are required to get gathering tools, having a continual demand for gathering tools equates to a continual usage of dungeons. I think the current durability for gathering tools would transfer over completely fine, especially if fewer raw materials would be required to level up due to profession quests.
        </p>
        <h4>
            QoL
        </h4>
        <p>
            Here are some other miscellaneous ideas that can help the prof experience. These are some other tweaks I would recommend, but aren’t super important relative to my core ideas:
        </p>
        <ul>
            <li> Add Gathering XP and Crafting XP potions to Zhight Island and rarely in high-tier chests </li>
            <li> Make the first few profession quests each day give bonus XP </li>
            <li> Also have raw materials (e.g. Oak Paper) on the bazaar </li>
            <li> List how to acquire ingredients when they are searched on the bazaar </li>
        </ul>
        <h4>
            Conclusion
        </h4>
        <p>
            I believe these modifications to Wynncraft would completely fix professions. Most experienced players agree that something needs to be done with professions, and this is my suggestion. Even if you don’t agree with my critiques or solutions, I hope that this analysis sparks a larger discussion to help improve Wynncraft.
        </p>
        <h3>
            Member Shout of the Publication
        </h3>
        <p>
            <TT.Name>
                kloweechu
            </TT.Name>! For incredible contribution recently
        </p>
        <p>
            <strong>
                Trivia Fact
            </strong>
        </p>
        <TT.TriviaFact>
            The Eiffel Tower can be 15 cm taller during the summer, due to the expansion of the iron in the heat.
        </TT.TriviaFact>
        <h3>
            Closing Remarks
        </h3>
        <p>
            Concluding this edition of the Titan Times has been an absolute thrill ride! Crafting each section was a joy, and I sincerely hope you found as much delight in reading it as I did in writing it. An enormous shoutout to my fellow Brilliance Cabinet members for their unwavering support and invaluable feedback throughout this process. Keep your eyes peeled for our next installment, packed with fresh updates and thrilling adventures awaiting your discovery. Until then, may your quests be filled with excitement and your victories abundant, Titans!
        </p>
        <TT.Signature>
            -C0rrupted
        </TT.Signature>
    </>);
}

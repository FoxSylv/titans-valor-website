import * as TT from '../NewsComponents';

export default function Page() {
    return (<>
        <h2>
            Recent Happenings 📅
        </h2>
        <p>
            <TT.Name>
                Kloweechu
            </TT.Name> has been hosting a bunch of giveaways! She has given away mythics, Wynncraft gift cards, and even more mythics! Don’t forget to react to the announcements to have a chance to win. 
        </p>
        <p>
            Gather around, movie lovers! Our guild has been hosting a delightful series of movie nights that have quickly become a favorite guild activity. Each event is an opportunity for members to unwind, chat, and enjoy a film together.
        </p>
        <p>
            We host scheduled movie nights where everyone is invited to join the fun. Simply hop into our designated voice channel on Discord to watch the movie with fellow guild members. It's a great way to relax and bond with your guildmates over some popcorn and great films. Don't just watch movies—help us pick them! We run polls where you can vote for the next movie we watch. If you have a favorite film or a genre you're itching to see, make sure to drop your suggestions in our Google Form. Your input helps us ensure that everyone gets a chance to watch something they enjoy.
        </p>
        <p>
            So far, we've enjoyed a variety of films including <strong>Madagascar</strong>, <strong>Kingsman</strong>, <strong>Baby Driver</strong>, and more! Each movie night has been a hit, providing both entertainment and a chance to discuss the films in-depth afterward. Join us for the next movie night and bring your snacks! It's a perfect way to enjoy some downtime with your guildmates and maybe discover your new favorite film.
        </p>
        <p>
            Now on to the promotions! Help me congratulate <TT.Names>
                ChaoticGlitched, livingful, JuanTheShoe, suzukzmiter, RedPandaas, MrRickroll, WanPunch, Jk725, Anno9474, 0lz, Talkair, Arc_ne, and howl888
            </TT.Names> on Warlock. Huge congratulations to <TT.Names>
                Evanfunny, C00D, thatlamppostguy, LososCroissant, and Zepart
            </TT.Names> on Artificer. Each of you has shown exceptional dedication and skill, and your promotions are well-deserved. Keep up the excellent work! We look forward to seeing how each of you will further contribute to our guild's success in your new roles!
        </p>
        <h3>
            What’s in the Cupboard?: ANO Cabinet Review
        </h3>
        <h4>
            Spirits
        </h4>
        <p>
            Spirit Cabinet outdid themselves this time, by hosting an exhilarating Easter Egg Hunt that brought together the community in a spirited challenge of exploration and agility. Participants dashed across a specially designed map, solving puzzles and doing parkour to collect hidden eggs. The competition was fierce but friendly, with everyone eager to snag the top prizes.
        </p>
        <p>
            Congratulations to <TT.Name>
                Mindless
            </TT.Name> for clinching first place with an impressive 91 eggs! Kudos also go to <TT.Names>
                EASYgamesSK, Gebuttertewurst, C00D, Momentums, FoxSylv, leSnowey, and linnyflower
            </TT.Names> for their fantastic performances and egg-finding skills. A big thank you to all who participated and made the event a memorable one. 
        </p>
        <TT.PhotoGallery>
            <TT.Image src="/news/vol14/egg1.png" alt="Egg hunt photo" />
            <TT.Image src="/news/vol14/egg2.png" alt="Egg hunt photo" />
            <TT.Image src="/news/vol14/egg3.png" alt="Egg hunt photo" />
            <TT.Image src="/news/vol14/egg4.png" alt="Egg hunt photo" />
        </TT.PhotoGallery>
        <TT.Centered>
            <p>
                Some lovely photos from our Easter event!
            </p>
        </TT.Centered>
        <p>
            The Wynncraft Photography Contest invited participants to capture the essence of the Wynncraft world through their lenses. Our guild members explored far and wide, taking stunning screenshots that showcased everything from serene landscapes to bustling town squares. The contest concluded last week, with submissions judged on atmosphere, look, color, and creativity. This event not only highlighted the scenic beauty of Wynncraft but also brought our members together in a shared artistic endeavor.
        </p>
        <TT.PhotoGallery>
            <TT.Image src="/news/vol14/photo1.png" alt="Nemract Cathedral at twilight" />
            <TT.Image src="/news/vol14/photo2.png" alt="Galleon's Graveyard at sunrise" />
        </TT.PhotoGallery>
        <TT.Centered>
            <p>
                First and second place from our photography contest!
            </p>
        </TT.Centered>
        <p>
            Similarly, the Wynncraft Writing Competition allowed our members to express their storytelling skills. Participants submitted pieces ranging from epic poems to immersive short stories, all set within the diverse realms of Wynncraft. This competition was a fantastic opportunity for writers to showcase their narrative abilities and for the community to engage with new and original lore-inspired content. Judged by a panel of our Spirit Cabinet members, the competition was a resounding success, celebrating the creative spirit that thrives within Titans Valor.
        </p>
        <p>
            If you missed out this time, don't worry—there are always more opportunities on the horizon to showcase your talents and maybe even win some great prizes!
        </p>
        <blockquote>
            If these events have sparked your interest or if you're keen to play a role in shaping future adventures, consider joining the Spirits Cabinet! All current members are welcome to apply. Getting involved in a cabinet not only enhances your chances of promotion but also offers you a platform to unleash your creativity. The sky's the limit when it comes to what we can achieve together. So, if you're ready to contribute your ideas and energy to our vibrant community, the Spirits Cabinet is waiting for you!
        </blockquote>
        <h4>
            Brilliance
        </h4>
        <p>
            The Brilliance Cabinet has been hard at work over the past month, with a command overhaul and a complete rewrite of the website. <TT.Name>
                FoxSylv
            </TT.Name> has revamped the guild website, adding mobile support, up-to-date leaderboards, and has many more features planned for the future. Check it out at <TT.Link href="https://titansvalor.org">
                titansvalor.org
            </TT.Link>! Meanwhile, <TT.Name>
                C0rupted
            </TT.Name> has completely overhauled the profile command, enhancing its appearance and providing more relevant information.
        </p>
        <TT.Centered>
            <TT.Image src="/news/vol14/profile.png" alt="Profile command result" />
            <p>
                The profile command overhaul!
            </p>
        </TT.Centered>
        <blockquote>
            As always, we're on the lookout for fresh talent to join our ranks and contribute to our ever-growing creative endeavors. Whether you're a seasoned coder, or simply brimming with innovative ideas, there's a place for you in Titans Brilliance. Don't hesitate to reach out and get involved – together, we can continue to push the boundaries of creativity in Wynncraft!
        </blockquote>
        <h3>
            Military Updates ⚔️
        </h3>
        <p>
            As we wave farewell to a thrilling Season 17 and gear up for the onset of Season 18, let's take a moment to reflect on our recent achievements and set our sights on the new challenges ahead. 
        </p>
        <p>
            Season 17 was marked by fierce competition and remarkable teamwork, culminating in some outstanding performances that deserve recognition. A special congratulations to <TT.Name>
                Zepart
            </TT.Name>, who clinched the title of Champion of the Ocean! <TT.Name>
                Zepart
            </TT.Name> showed his dedication with a total warcount of 2655, truly setting a high standard.
        </p>
        <p>
            Excitingly, this new season also introduces some innovative war builds that are sure to change the dynamics on the battlefield. The new Hadal DPS build has been optimized for maximum damage output, ideal for those who like to strike hard using Shaman. Meanwhile, there is the Lament Solo build for those who prefer to control the field solo. These builds have been meticulously developed to adapt to the evolving war strategies and enemy tactics we've been encountering. 
        </p>
        <p>
            Looking forward, Season 18 promises to bring new opportunities and challenges. We are eager to see how new and veteran members alike will rise to the occasion. FFAs are set to be the same as last season, however keep an eye out for any changes in the military announcements channel in our discord. As we continue to navigate the shifting tides of alliance and rivalry, our focus remains on strengthening our strategic positions and enhancing our collective skills. 
        </p>
        <p>
            Last but not least, let's congratulate <TT.Names>
                C00D, C0rupted, Zepart, and LososCroissant
            </TT.Names> for passing the strategist test. Their hard work and dedication to mastering the complexities of military strategy have paid off, earning them well-deserved recognition as strategists within our guild. Well done to them all! Your success bolsters our military prowess and sets a fantastic example for upcoming strategists in our ranks.
        </p>
        <p>
            Let's set sail into Season 18 with determination and perseverance, ready to defend our legacy and claim new victories!
        </p>
        <h2>
            Main Stage: A review of Wynncraft’s Dungeons
        </h2>
        <blockquote>
            Written by thatlamppostguy
        </blockquote>
        <p>
            Hello everybody! Welcome to the Main Stage of this edition of Titan Times. Today I’m going to be babbling about dungeons! An essential part of every Wynncraft playthrough, and one of the first pieces of content added to the game. I’ll be reviewing each one and giving it a score out of 10. I won’t cover corrupted dungeons as for the most part they’re just harder versions of the original, though I will mention them if I think they’ve done something that improves upon the original. Well without further ado let us begin!
        </p>
        <h5>
            Decrepit Sewers
        </h5>
        <p>
            First up, unsurprisingly, the first dungeon in the game: Decrepit Sewers! 
        This dungeon is definitely one of the best, being a solid introduction to the concept of dungeons. It's simple and quick and gets the job done. The rooms are ultimately just bare bones parkour and collection rooms and the boss is nothing too special (though it does have a couple cool mechanics such as the barriers that move up and down to provide temporary cover), however this really doesn't detract from it, it's the bare bones nature of it that makes it work in my opinion. It's very repeatable (as I'm sure Mr LamDaKis will tell you) and overall a great dungeon. 8/10! 
        </p>
        <TT.Centered>
            <TT.Image src="/news/vol14/sewers.png" alt="Decrepit Sewers with shaders" />
        </TT.Centered>
        <h5>
            Infested Pit
        </h5>
        <p>
            Alrighty, Infested Pit, an arachnophobe's worst nightmare. This dungeon builds on the ideas set up by DS, with longer and more complex parkour and once again more collection rooms. Now one thing these collection rooms share with DS is that ever since 2.0.3, they don't actually require you to pick up tokens and wait an eon being attacked by mobs while they fill up, instead they work on more of a kill count system which is just sooo much nicer and definitely elevates these 2 because of it. Why this hasn't been added to later dungeons I don't know but I sure hope they do. Back on track to Infested Pit, one thing that does make this dungeon worse: COBWEBS! You get stuck in them and man it's just so annoying (and yeah ik this is likely just a skill issue on my part). Overall Infested Pit is an ok dungeon, nothing amazing but nothing too terrible, just plain average in my opinion.  5/10! 
        </p>
        <h5>
            Underworld Crypt
        </h5>
        <p>
            Next up is Underworld Crypt, definitely elevated by none other than the man himself GENERAL GRAKEN! What a guy. This dungeon is definitely up there, with uniquely designed rooms that just don't devolve to token collection.There’s the survive the platform room, which is a cool idea that I’m glad they added to at least one dungeon.  The ferry room is also a personal favourite of mine, being a nice nod to the mythology of Charon. Talking of Charon, he does kind of let the dungeon down, the whole boss is just smack him and kite. I do wish they would  improve the boss somehow, with more unique mechanics and nods to the mythology. Definitely still a good dungeon and one of my favourites, even if the boss does hold it back somewhat. 8/10! 
        </p>
        <TT.Centered>
            <TT.Image src="/news/vol14/crypt.png" alt="Underworld Crypt with shaders" />
        </TT.Centered>
        <h5>
            Timelost Sanctum
        </h5>
        <p>
            And now ladies and gentlemen the newest dungeon in the game, Timelost Sanctum! This dungeon was added in 2.0.3 to replace the abomination that was Lost Sanctuary (sorry lost sanctuary fans, if there are any). Timelost Sanctum is certainly a step up from its predecessor, once again escaping the trap of token rooms and fitting with the theme of time valley a lot better. I really like how this dungeon is designed, with the 4 separate areas that come together for a final boss. Can't wait for the corrupted version and the unlocking quest, CT did a great job with this one which gives me hope for dungeons to come and dungeons to be reworked. 8/10! 
        </p>
        <h5>
            Sandswept Tomb
        </h5>
        <p>
            Moving onwards to Sandswept Tomb, dungeon de la desert. This dungeon is one of the better ones in my opinion, with unique features such as a variety of rooms that you can get for various challenges, similar to later raids, which is a really cool idea which adds to its replayability. While it does unfortunately succumb to the curse of the dreaded token rooms, it doesn’t detract from the experience too much, as only a couple of them are straight up just collectors, with the first being a kill mobs up to a boss, adding some more variety to the token rooms. A lot of people do hate on the parkour in this dungeon, which is fair enough. Several of the parkour rooms in this dungeon are absolute torture, however some I do think are good ideas, such as the mirror parkour room. Another thing that does somewhat detract from this dungeon is barriers once again in a couple of the rooms, and the wall traps are really kind of annoying in rooms where you can’t dodge them. Overall, a great addition to Wynn’s dungeons. 7/10!
        </p>
        <TT.Centered>
            <TT.Image src="/news/vol14/sand.png" alt="Sandswept Tomb with shaders" />
        </TT.Centered>
        <h5>
            Ice Barrows
        </h5>
        <p>
            Oh boy, Ice Barrows. This is gonna be a long one. Someone's gotta have the title of worst dungeon and man is it this one (though fallen factory does give it a run for its money but we'll talk about that later). Just to spice things up a bit and for the sake of this paragraph not being a page long I’m gonna list what I hate about this dungeon instead. 
        </p>
        <p>
            Drumroll Please:
        </p>
        <ul>
            <li>
                Ice parkour is a pain
            </li>
            <li>
                The barriers in the running room are really annoying, hell that whole room is, you can barely spellcast for mobility because of the aforementioned barriers and then if you’re not quick enough it dumps an ice wall in your face that you have to wait behind for 30 seconds or so
            </li>
            <li>
                The first room sucks to navigate, so many fences and odd blocks that you just keep hitting and getting stuck behind and it’s just so annoying, not to mention TOKENS!
            </li>
            <li>
                This dungeon also stays in character with sucking by having the WORST token room in the game, 32 tokens required, which isn’t so bad due to the abundance of mobs however having to sit there while they fill 1 by 1 whilst being knocked all over the place is incredibly annoying
            </li>
            <li>
                The final room is actually ok, however there is one thing that is just so annoying and unnecessary it fits perfectly. If you have more than one Blue Token in your inventory, it only registers putting in one. That’s right, the other just vanishes and you have to do it again. Because Wynncraft.
            </li>
        </ul>
        <p>
            The only reason this dungeon isn’t a straight 1/10 is because of the quest to unlock it (Fate of the Fallen) is actually pretty cool with some nice additions to the lore, and Theorick’s sacrifice at the end is honestly a pretty good end to the lore around him. And no, I won’t be mentioning quests from here on out, Ice Barrows really just didn’t have anything else good going for it. Sorry but not sorry to the one Ice Barrows fan out there. 2/10.
        </p>
        <TT.Centered>
            <TT.Image src="/news/vol14/barrows.png" alt="Ice Barrows entrance with shaders" />
        </TT.Centered>
        <h5>
            Undergrowth Ruins
        </h5>
        <p>
            Moving on from utter torture, Undergrowth Ruins, slime extravaganza! This dungeon is pretty alright, unfortunately falling trap to the token collection rooms once again, though it does spice it up a bit with the token tower room, which is nice I guess. The Parkour in UR is actually in my opinion quite fun, especially in the corrupted version, which adds the arrow boosters that require you to right click, which adds more of a challenge, though not to the point of being infuriating. Slykaar, the boss, is also pretty cool in the fact that it isn’t just a straight beat ‘em up, with the slime phases at the beginning splitting down into him adding some variety. This is once again improved upon slightly in the corrupted version, with the slime bounce tower to finish Slykaar off at the end. Overall, a pretty solid dungeon. 6/10!
        </p>
        <h5>
            Galleon's Graveyard
        </h5>
        <p>
            Next stop, the last dungeon for a fair few levels, Galleon’s Graveyard. I have to say I do quite like the Pirate Aesthetic in the rooms, as while it does succumb to the dreaded tokens, it does add a unique spin on them, such as having to swim away from a giant piranha, or having a final boss at the end. The Cannon room, while it can be a pain in the ass to play if it's laggy, is a very cool idea that can be quite fun under the right circumstances! The first room can be quite annoying due to the sprint bar (never forgiving you for that one salted) it is at least unique. One thing the corrupted dungeon adds is the funny corrupted cannon, and the rocket jump parkour before the boss, not too special but it is pretty fun, making it a solid addition. The boss is also pretty cool, probably having the most different mechanics so far, with the tnt launching phase in between just smacking poor old redbeard. This dungeon  definitely stands out a little from the others thanks to its unique mechanics and rooms and is pretty fun too. 8/10!
        </p>
        <TT.Centered>
            <TT.Image src="/news/vol14/graveyard.png" alt="Galleon's Graveyard entrance with shaders" />
        </TT.Centered>
        <h5>
            Fallen Factory
        </h5>
        <p>
            And after a 20 level hiatus (seriously why are there no lvl 70-90 dungeons) Fallen Factory, yay… Now, this dungeon isn’t the best, however there is one thing that keeps it from Ice Barrows territory. I don’t know if this is just me, but funny computer’s sarcasm does make me chuckle. Quite possibly the only thing keeping me sane while I try to get through the production line room though, I’m not sure if it’s just a skill issue on my part or what, but I just keep failing and failing. It takes me at least 5-6 tries every time. Moving swiftly onwards from my massive skill issue, the rest of Fallen Factory is pretty much a glorified grind room, even the boss. It probably falls into the trap of the token room the most out of any, which does lead to it being kind of boring. One thing I do like in this dungeon however, is the parkour room. While it confused me at first, I like how this is basically the only parkour in the game that actually takes into account that you have movement spells. There’s something satisfying about just flying around from cog to cog, doing whatever the hell you want. Unfortunately though, this dungeon does fall short in my opinion, so sorry Fallen Factory fans. 3/10!
        </p>
        <h5>
            Eldritch Outlook
        </h5>
        <p>
            Our final stop of this review, Eldritch Outlook. The last dungeon (as of right now) in Wynncraft. Does it deliver? Well, short answer, ABSOLUTELY. This is hands down the best dungeon in the entire game, and I will die on this hill. Every room takes concepts previously established in earlier dungeons and adds unique spins on them that keep the dungeon interesting and fun right up until the boss. Yes, it does fall trap to the token rooms which I have been so disgusted by so far, however, it still finds ways to make them interesting. The mirror room requires you to hop from either side to get all the tokens, and the boss rush room is quite possibly my favourite room in any dungeon. There’s something so satisfying about blitzing through 5 consecutively harder bosses in a row (seriously, Wynncraft really needs to add some boss rush content). Now, the parkour room does get some hate, and while I see where they’re coming from, I don’t think it’s enough to really degrade this dungeon. Personally, I think it’s a really unique concept for a parkour room, and after the first few times, it’s honestly not that bad at all. And even if you are suffering through the parkour room, the OSTs will keep you sane. I haven’t mentioned OSTs before in this review, because for the most part it’s nothing too standout. But the OSTs for Eldritch Outlook are just amazing. Genesis of the End and Everpresence are two of the best tracks in the entire game. I can’t help but bob along to them every time I run this dungeon. Finally, we move onto the boss, The Eye, Overseer of Wynn and the final boss of Wynncraft (as of right now). Yes, there are harder bosses, but in terms of lore and build-up The Eye is definitely the final boss. It is by far the best dungeon boss in the game, with different phases, unique mechanics such as the eye laser, falling eye dodgeball and laser dodgeball, the attack where it goes all Eye of Cthulu and tries to eat your face, and the final stage where the Iris jumps out and attacks you. Seriously this is one hell of a boss fight, utter chaos and incredibly fun. This dungeon is an absolute masterpiece. 10/10. 
        </p>
        <TT.Centered>
            <TT.Image src="/news/vol14/outlook.png" alt="Eldritch Outlook with shaders" />
        </TT.Centered>
        <p>
            Congratulations! You have reached the end! Well done on surviving 11 whole paragraphs of me babbling about dungeons in a block game. I hope you found it entertaining and didn’t think I was spitting complete rubbish the whole time. Thank you very much for reading!
        </p>
        <h3>
            Member Shoutout of the Publication
        </h3>
        <p>
            <TT.Names>
                LososCroissant and CrispperPlanet
            </TT.Names> for their extensive GXP contributions!
        </p>
        <p>
            <strong>
                Trivia Fact
            </strong>
        </p>
        <TT.TriviaFact>
            The world's oldest known recipe is for beer. It dates back to around 3,900 BCE and was found on a Sumerian clay tablet in Mesopotamia (modern-day Iraq).
        </TT.TriviaFact>
        <h3>
            Closing Remarks
        </h3>
        <p>
            Writing this edition of the Titan Times has been a delightful journey! A heartfelt thank you to everyone who contributed their insights, and an especially big shoutout to our Brilliance Cabinet for their crucial feedback and support throughout the drafting process. Thanks also to <TT.Name>
                thatlamppostguy
            </TT.Name> for your main stage. Your dedication helps make each edition better than the last. Keep an eye out for our next issue, packed with even more guild news and exciting features. Until then, happy adventuring in Wynncraft, and thank you for your continued support and enthusiasm for the Titan Times. Let's keep the community spirit alive and thriving!
        </p>
        <TT.Signature>
            -C0rrupted
        </TT.Signature>
    </>);
}

import * as TT from '../NewsComponents';

export default function Page() {
    return (<>
        <h2>
            Recent Happenings 📅
        </h2>
        <p>
            First things first, I am beyond happy to announce that our guild is now at level 105. Let us all give the biggest thanks to <TT.Name>
                GebbutterteWurst
            </TT.Name>, who hosted the “ANO guild exp grinding event” on the 23 June. We appreciate all the ANO members who participated to make this happen. More information about this event will be provided within the cupboards of ANO, so keep an eye out sailors!
        </p>
        <p>
            The seas are buzzing with talk about the upcoming rekindled update. The wynn team describes this new update as their most colossal one ever. With the beta out, much enthusiasm has been shared of what the update will bring to wynn. Be prepared titans, this update will bring all sorts of changes that will be sure to shake the seas of wynn.
        </p>
        <p>
            Now unto community promotions!
        </p>
        <p>
            For warlock, let us all congratulate <TT.Names>
                ScientificSnake, zSimpbeartje_, 28culture, __Martyy, Stencill, Hibikies, and Mayonnee
            </TT.Names> for their hard work within the guild. We hope to see them all sail smoothly to new heights.
        </p>
        <p>
            As for Artificer, let us give a big round of applause for <TT.Names>
                Waiz, anAlarmingAlarm, Overfears, Kioabc1, Kevinboiii, howl888, AspectOfThePepe, Inuke__, and Xrlylyl
            </TT.Names> for their spectacular efforts and contributions in the guild. You have all navigated to reach amazing heights, and we hope to see you all continue your journey up the ANO ranks.
        </p>
        <h3>
            What's in the Cupboard?: ANO Cabinet Review
        </h3>
        <h4>
            Spirits
        </h4>
        <p>
            As always, our Spirits crew made a big splash with their efforts and outstanding events. Like dedicated sailors, they are always making sure our community flows smoothly and interactively.
        </p>
        <p>
            Sailors, I am beyond thrilled to announce that ANO has finally reached level 105! Huge thanks to <TT.Name>
                GebbutterteWurst
            </TT.Name> who hosted the ANO guild exp grinding event in order to set the guild on the path to levelling up to level 105. The team included <TT.Name override="GebbutterteWurst">
                Geb
            </TT.Name> himself, alongside members of the guild, some of which provided insane contributions: props to <TT.Names>
                CrispperPlanet, LososCroissant, zSimpbeartje_, LaMDaKiS, thatlamppostguy, __Martyy, Calluum, howl888, and OverFears
            </TT.Names> for all of their contributions. 
        </p>
        <p>
            With their efforts, this event managed to sail to success. Let’s give them all a huge wave of applause! This could not have been done without the cooperation of so many members.
        </p>
        <p>
            In addition, this event also had a raffle. Rewards included lots of LE, a mystery mythic, a white horse, and a Well of Power (festival of the spirit utility trinket). The winners of the raffle were later revealed to be: <TT.Name>
                CrispperPlanet
            </TT.Name> in 1st place winning the mystery mythic, <TT.Name>
                zSimpbeartje_
            </TT.Name> in 2nd place winning 1stx of LE, and in 3rd place winning 40LE was <TT.Name>
                LososCroissant
            </TT.Name>. You can check out the loot of the winners in the #events channel in our discord.
        </p>
        <TT.Centered>
            <TT.Image src="/news/vol15/level105.png" alt="Guild level 105 announcement" />
        </TT.Centered>
        <p>
            Sailing on, <TT.Name>
                GebbutterteWurst
            </TT.Name> also hosted a ‘’Wynncraft snap’’ event where players had to run around the wynncraft provinces to take screenshots of mobs who met certain criterias, to earn themselves points. There were 5 categories and all 5 had different points. Keep in mind that they had to do all of this in under 90 minutes!
        </p>
        <p>
            Paired up with this event was a mystery raffle, where the prize was later revealed to be an Absolution claimed by <TT.Name>
                LososCroissant
            </TT.Name>. 
        </p>
        <p>
            Apart from the raffle, the winners and their respective prizes from the event were: in 1st place <TT.Name>
                Biblonko
            </TT.Name> who won 1stx of le, followed by <TT.Name>
                linnyflower
            </TT.Name> in 2nd with 32le, and <TT.Name>
                MrRickroll
            </TT.Name> in 3rd place with 16le. Players with 100 points each received an additional 4le!
        </p>
        <TT.PhotoGallery>
            <TT.Image src="/news/vol15/event1.png" alt="Event in progress" />
            <TT.Image src="/news/vol15/event2.png" alt="Event in progress" />
            <TT.Image src="/news/vol15/event3.png" alt="Event in progress" />
            <TT.Image src="/news/vol15/event4.png" alt="Event in progress" />
        </TT.PhotoGallery>
        <p>
            Moreover, <TT.Name>
                thatlamppostguy
            </TT.Name> hosted an event named the “Wynncraft Speed Levelling Competition’’, and exactly like the name suggests, players competed in a battle of who could level up the fastest under only an hour! First place was taken by <TT.Name>
                MrRickroll
            </TT.Name> claiming his treasure of 1stx of le, followed closely behind by <TT.Name>
                linnyflower
            </TT.Name> in 2nd who claimed 40le, and in 3rd was <TT.Name>
                __Martyy
            </TT.Name> claiming 20le.
        </p>
        <p>
            Some other events included a ‘’Housing survey’’ by our council member Micah, who hosted a survey with a vast list of questions for all members. After receiving the surveys, he rewarded the non-homeless members of ANO on wynn (<TT.Names>
                Talkair and FoxSylv
            </TT.Names>).
        </p>
        <p>
            There were also some giveaways, namely by <TT.Name>
                Zepart
            </TT.Name> (4stx giveaway for 4 winners), by <TT.Name>
                Magumu
            </TT.Name> (1stx giveaway for 1 winner), and <TT.Name>
                Deniour
            </TT.Name> (2tsx giveaway for 1 winner).
        </p>
        <p>
            This concludes the end for the spirits section. Huge thanks to the spirits members who hosted all of these events. This is a reminder for you to go and get on board by selecting the event role in #role-react to stay afloat with all the events happening inside ANO. The team also always looks forward to new members joining, so give it a try and apply for a role inside spirit if your passion lies with hosting events for the community!
        </p>
        <h4>
            Brilliance
        </h4>
        <p>
            Next up, let's hoist the anchors and sail towards news at what our brilliance team has been up to. They’re always working their best to make sure they’re delivering the highest quality of content, and this time they did not disappoint!
        </p>
        <p>
            First things first, the talented <TT.Name>
                Aezuh
            </TT.Name> has worked on the Valor bot’s <code>-coolness</code> command, changing the visuals to be a tad bit more pleasing to the eyes. Let's just say that this new look is as “cool” as a fresh cold coke (laugh with me pls).
        </p>
        <TT.Centered>
            <TT.Image src="/news/vol15/newcoolness.png" alt="The new coolness command" />
        </TT.Centered>
        <p>
            Our next point is to take a sneak peak at what <TT.Name>
                FoxSylv
            </TT.Name> is currently cooking up. With her brilliant skills for tech, she is zooming the seas by actively developing a map for our very own website!I can't spill all the secrets just yet, but keep your eyes peeled for this thrilling new feature!
        </p>
        <TT.Centered>
            <TT.Image src="/news/vol15/newmap.png" alt="Map sneak peek" />
        </TT.Centered>
        <p>
            Don’t forget to keep an eye out for new updates by the brilliance team sailors! You will not want to miss out on anything. Let us give a huge applause for all who have and are constantly working on improving ANO with their constant efforts within Brilliance.
        </p>
        <p>
            With that being said, our journey within the cabinets has now concluded. Both these teams always welcome new applicants so don’t hesitate to apply if you want to lend an extra hand.
        </p>
        <h3>
            Military Updates ⚔️
        </h3>
        <p>
            As we lowered the sails and bid farewell to season 18, we gave a huge round of applause to everyone who participated. Although not our most active season, we always appreciate our members’ participation. Congratulations to <TT.Name>
                Talkair
            </TT.Name> who claimed the season’s “Ocean trials - Champion of the Ocean'' title, followed by <TT.Name>
                anAlarmingAlarm
            </TT.Name> in 2nd place and <TT.Name>
                thatlamppostguy
            </TT.Name> in 3rd. 
        </p>
        <p>
            Setting sail into season 19, we hope to see a thrilling sight of soldiers fighting to claim the title of ‘’Champion of the ocean’’. Ocean’s blessing soldiers!
        </p>
        <p>
            Hoist the anchors as we prepare for the epicness which are promotions:
        </p>
        <p>
            We give a huge wave of applause to <TT.Name>
                Eshreal
            </TT.Name> who has earned herself the rank of Chief! Eshreal has done wonders for the guild, and has been here to defend the guild on multiple occasions by herself. With her vast knowledge of eco, we are sure to thrive with her as one of our Chiefs.  Once again, huge congratulations to you Eshreal, we wish you an amazing journey with your new-found responsibilities.
        </p>
        <p>
            Let’s now move on to our Strategist promotions. A huge congratulations to <TT.Names>
                thatlamppostguy, WaiZ, Kioabc1, Talkair, and Kevinboiii
            </TT.Names> for passing the strategist test. By passing this test, you have all proved that you have what it takes to be a backbone of ANO in the eco department. We wish you all success for the future!
        </p>
        <h2>
            Main Stage: A look at the races of Wynncraft
        </h2>
        <blockquote>
            Written by thatlamppostguy
        </blockquote>
        <p>
            Hello, hello Titan Times readers, I’m thatlamppostguy and today I’m going to be discussing the races of Wynncraft, from plain old Humans to Doguns and Avos. Wynn obviously has your typical fantasy races: Humans, Elves, Dwarves and Dragons, but also less common races such as Hobbits and Gnomes. But it’s the unique races created by the CT that really shine. So let’s go through them all and discuss aspects I think we're done well and not so well.
        </p>
        <h3>
            Humans
        </h3>
        <p>
            Let’s start off with plain old Humans. Humans are often overlooked as just the basic race but they still have their own unique characteristics which I think were portrayed really well in Wynncraft, the Humans of Wynn are hardy folk, they’ve survived a millennia of war against the forces of the undead. While this has led them to having little to no moral restraints (especially in the case of the player) in doing whatever necessary to achieve the greater good, usually through violence, they do it with the best intentions. Humans are one of my favourite races in the game, and while I am biased because, well I’m a Human myself, I think this is definitely in part due to how Humans were characterised. 
        </p>
        <TT.Centered>
            <TT.Image src="/news/vol15/ragni.png" alt="Ragni at sunrise with shaders" />
        </TT.Centered>
        <h3>
            Villagers
        </h3>
        <p>
            Next up Villagers, while created by Mojang originally, basically all ideas of them as a fantasy race were created by the Wynncraft CT, so I’ll be counting them as a mostly original race. I like the Villagers- well actually I don’t, they’re kind of racist pricks half the time, discriminating against basically anyone who isn’t a villager, even their allies in Humans. But I do like how the CT took traits present in villagers in regular Minecraft, and kind of expanded on them. Their love of emeralds leads them to be greedy at times, their isolationism leads them to be kinda xenophobic. 
        </p>
        <h3>
            Elves
        </h3>
        <p>
            Elves, the slightly snobby forest dwelling race with an eternal lifespan- as depicted in most fantasy worlds, including Wynn. However once again the CT have added a unique spin on them- being an obsession with hallucinogenics and the worship of “The Light '' (a giant dog with antlers). While the hallucinogenics obsession is more of a joke, the worship of light really helps flesh them out, as well as the world. It even influences their decisions *cough* *cough* accidentally starting the War of the Realms (little stuff like that). Another really cool addition associated with Elves is the outcast town of Efilim, the Elves of Aldorei are sticklers for rules, and any who break them are cast out. Efilim really didn’t need to exist but it does, and it really helps to make the Elves feel more alive and part of the world in my opinion.
        </p>
        <TT.Centered>
            <TT.Image src="/news/vol15/aldorei.jpg" alt="Aldorei at night with shaders" />
        </TT.Centered>
        <h3>
            Dwarves
        </h3>
        <p>
            Ah, Dwarves, the first race I’m going to be quite critical of. In comparison to pretty much any other fantasy race of dwarves, Wynncraft's dwarves just fall short. They just lack any real sense of dwarfiness, even the Wynncraft-ey spin that nearly all other races have- they live underground sure, but other than that they have basically no other discernible characteristics. They don’t seem to have any love for mining and crafting, or anything else generally associated with dwarves, and they don’t even have any other traits unique to Wynncraft’s interpretation of them- unless you count being genocidal maniacs, which is mostly due to brainwashing and not even a quality of their race anyway. It may seem like I’m bashing on the dwarves a bit too much, and yes maybe I am, but I’m kind of a big fan of dwarves in most other fantasy worlds, so this really does seem like a let down.
        </p>
        <h3>
            Greenskins
        </h3>
        <p>
            Greenskins? What the hell are those? Greenskins is a name used in the Order of the Grook to describe Orcs, Goblins, Hobgoblins, etc. But as the whole list of groups  that’s quite a lot to be repeatedly typing I’m going to be referring to them as Greenskins. I’m also going to be grouping them together as they’re all pretty similar, with the only real variation being in size and intelligence. Now I really like how Greenskins are portrayed, they’re antagonistic at first, as they are in basically all fantasy settings. However it’s ultimately revealed that they’re really decent people, if a little violent at times, and really it’s the villagers who’ve been more antagonistic the whole time (this is what I mean about them being kinda racist). This is a fairly unique and amazing Wynncraft-ey take on Orcs and Goblins and it really fits into the world.
        </p>
        <h3>
            Olm
        </h3>
        <p>
            The Olm, the first race on this list created entirely by the CT- and I think they did a really good job. The Olm are mysterious and have bizarre unexplainable powers; perfect for a long lost civilization. What we do know about them is enough for them to feel part of the world, and yet leaving us wanting more at the same time. They had some power over time itself, they were ultimately absorbed by darkness yet some escaped using cosmic magic. Is the cosmic magic related to time control? Are there any Olm left? What the hell is Old Man Martyn’s deal?? So many questions and in a good way. They’re purposely somewhat vague and it’s great. The Olm are excellently done and one of my favourite races in Wynn, and possibly my favourite unique race created by the CT. They do have a serious contender though:
        </p>
        <TT.Centered>
            <TT.Image src="/news/vol15/timevalley.jpg" alt="Time valley at sunset with shaders" />
        </TT.Centered>
        <h3>
            Doguns
        </h3>
        <p>
            There’s something about Doguns that just feels so right. They have their own culture and religion, they’re victims of a genocide, and few remain. But unlike the dwarves they fit perfectly into the environment of Molten Heights, they shaped it themselves after all. They’re creatures of Fire and Earth, like the volcanoes they inhabit the basins of, slow yet immensely powerful and old, nearly as old as the mountains themselves. They’re so suited to the environment of Molten Heights that if they come into contact with water they turn to stone. Doguns fit perfectly into Wynncraft’s world and story. An excellent job by the CT. 
        </p>
        <h3>
            Dragons
        </h3>
        <p>
            Continuing the theme of Fire, let’s talk dragons. Dragons don’t really play a major part in the story, they’re more background beings of another time, a bit like the Olm but a lot less important. The last living dragon, Ozoth does play a fairly significant role in saving the Canyon of the Lost from the Colossus, by gifting the player her Everflame, allowing them to free the Dogun Elders from Freezing Heights. While not major parts in the story, Dragons do still feel like they’re part of the world,they’re allegiances are unknown, but they do eventually help the player . To me they feel like a Dragon should feel, enormously powerful with ambiguous motives, more like a force of nature than anything- yet still able to be reasoned with and willing to help.
        </p>
        <TT.Centered>
            <TT.Image src="/news/vol15/ozoth.jpg" alt="Ozoth at sunset with shaders" />
        </TT.Centered>
        <h3>
            Avos
        </h3>
        <p>
            Another of the unique races made by the CT, the Avos are a great addition to Corkus. The contrast of the nature loving bird people with their druidic magic and the industrial Corkians with their mechs and robots is really well done, and makes the conclusion of them coming together more to defeat the Factory more satisfying. To be honest, I always found the Avos far more interesting than the mechs and Corkians. The druidic influences are really cool, and a lot more unique than just steampunk. Definitely a really underutilised area of Corkus, the Avos focused lootrun challenges are unique and interesting, and a more Avos focused raid or dungeon or anything would be really cool. Overall I really think the Avos were done well, they’re just a little overshadowed by the Corkians in my opinion
        </p>
        <h3>
            Gerts
        </h3>
        <p>
            Originally thought to be part of the Greenskins, Gerts are actually Villagers that have been warped by cosmic magic. This is a great way of developing cosmic magic by showing its effects on other races, and really shows the power it can have. It’s just such a shame that the Hunger of the Gerts quests just kind of cut off randomly (please fix this CT, it’s honestly such a massive shame, I think the quests have great potential if utilised correctly). The Gylia Plains secret discoveries still do a good job of explaining what Gerts are though, and the Quests do still manage to show some characteristics of the Gerts. Just please finish the quests I’m begging you CT aaaaah. 
        </p>
        <TT.Centered>
            <TT.Image src="/news/vol15/gertcamp.jpg" alt="Gert camp at night with shaders" />
        </TT.Centered>
        <h3>
            Wybels
        </h3>
        <p>
            Typical of Wynncraft to make the weird fuzzy rabbit things everyone keeps as pets interstellar space travelling hyper-intelligent aliens am I right? While Wybels being aliens is mostly a joke about conspiracy theories, it still kind of oddly fits into the world in a bizarre way. It is seemingly out of place yet not too out of place at the same time. Wybels being aliens even has some prior hints that don’t really make sense unless you know the whole story. The lore of the item Stardew shows that Wybels are often found gazing at the stars, and it’s also mentioned in various places that Wybels are drawn to cosmic magic- which makes the revelation that they’re aliens not entirely out of nowhere. The CT really didn’t have to add stuff like this but they did anyway, and it adds that little bit to the world feeling alive. 
        </p>
        <h3>
            Minor Races
        </h3>
        <p>
            This section at the end is kind of a grouping of the more minor races that aren’t really significant enough to have their own section. Gnomes, Hobbits, Nauter, the Jinko and other seemingly sentient races from the Canyon of the Lost, etc. I don’t have much to say about them, other than the addition of more minor races does help in fleshing out the world more, it  makes it feel like the world is still big and relatively unexplored. Although  perhaps a little more information about the culture and origin of these more minor races would be nice.
        </p>
        <TT.Centered>
            <TT.Image src="/news/vol15/squidvillage.jpg" alt="Squid village at night with shaders" />
        </TT.Centered>
        <p>
            Well, you’ve reached the end! Thanks for reading my little rant on sentient species in a fantasy minecraft game. I hope you found it interesting and feel free to yell at me if you think I got anything wrong!
        </p>
        <h3>
            Member Shoutout of the Publication
        </h3>
        <p>
            Shoutout to <TT.Name>
                Xrlylyl
            </TT.Name> for speedrunning Artificier
        </p>
        <TT.TriviaFact>
            In the English language, the word “rhythms” stands out as the longest word without a vowel. Despite its seemingly complex arrangement of consonants, it flows rhythmically off the tongue!
        </TT.TriviaFact>
        <h3>
            Closing Remarks
        </h3>
        <p>
            This unfortunately concludes the end of our voyage through the waves of ANO news. Thank you for trusting me as your captain! Writing this has been such an honour, and special thanks to <TT.Name>
                thatlamppostguy
            </TT.Name> for the incredible review for our mainstage. Don’t forget to brace yourselves for rekindled and keep an eye out for the next issue! Thanks for reading, and ocean’s blessing titans.
        </p>
        <TT.Signature>
            -Talkair
        </TT.Signature>
    </>);
}

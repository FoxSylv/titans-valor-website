import * as TT from '../NewsComponents';

export default function Page() {
    return (<>
        <h2>
            Recent Happenings 📅
        </h2>
        <p>
            Starting off, Patch #11 of the Spellbound Hero Beta was released earlier this month. This included the release of Shaman on the beta, plus a bunch of balance patches for assassin, and a handful of bug fixes (because Wynn is such a perfect game with absolutely 0 bugs
            <TT.Image src="/news/vol5/smile.webp" alt="" width={2} />
            ) Shaman mains rejoice as your time has come to test out the new content!
        </p>
        <TT.Centered>
            <TT.Image src="/news/vol5/shaman.png" alt="Biblonko as a shaman" />
            <p>
                Photo taken by Ugastag
            </p>
        </TT.Centered>
        <p>
            <strong>Beta is currently open to all players.</strong>
        </p>
        <p>
            Thanks to Ugastag, Biblonko, and BlueTheSniper, Titan’s Spirit has had quite the revival recently! With events such as mob hunts, Gartic Phone, item retrievals, and a variety of minigames, there’s lots to go around. Events are a fun way to bring guild members together both inside and out of Wynn. If you’re interested in attending events in the future, make sure you grab the events role in #guild-info so you get a ping when we announce new events!
        </p>
        <p>
            To add to that, we had an extra special event recently: Item bomb party! Thanks to Nimbuh, Cal, Halaiksse, and robopig3000, we were able to throw over 200 item bombs in a row (only 1 mythic resulted between over 20 people, big sad)! Personally, I didn’t get much worth mentioning, but I had fun hanging out with everyone!
        </p>
        <p>
            Did you know that BlueTheSniper has his own discord server dedicated to Wynncraft builds? Did you also know that Blue created an exclusive ANO role in his server that automatically grants all Titans Valor members VIP status? Did you want to have access to a huge database of all types of builds that are updated daily by other players? If any of these things sound good to you, make sure you head over to <TT.Link href="https://discord.gg/pBmcXY8H9U">
                Blue's Builds
            </TT.Link>.
        </p>
        <p>
            My favorite part of the publication has arrived: promotions! There have been quite a few since the last volume so don’t be too alarmed at the length of this list.
        </p>
        <TT.Indented>
            <p>
                <TT.Names>
                    Lucky32, Hothotrod, BlueTheSniper, bmarci04, CensingMarker51, HadesRPB, CatgirlZyris, OblivionEnjoyer, Masterfuzja, Emeraldglass, ItsEmerald, robopig3000, and _Constellar
                </TT.Names> have all been promoted to Sorcerer!
            </p>
            <p>
                <TT.Name>
                    Habidudi
                </TT.Name> has been promoted to Artificer!
            </p>
            <p>
                <TT.Names>
                    Biblonko and Ugastag
                </TT.Names> have been promoted to Titan! <TT.Image src="/news/vol5/llama.webp" alt="" width={2} />
            </p>
            <p>
                A couple of big promotions here, so congratulations to everyone on this list! Hope to see more new names up here next time!
            </p>
        </TT.Indented>
        <h2>
            Military Updates ⚔️
        </h2>
        <p>
            While this section might look a bit shorter, smaller in size does not mean decreased importance! Guild season 7 just ended; ANO earned just over 6 million SR points, putting us 6th overall! Congrats to everyone who participated in warring this season.
        </p>
        <p>
            With the season ending, that also means the end of another season of Ocean Trials come and gone! For anyone who doesn’t already know, Ocean Trials is an event we hold every season in which you can war for LE. At the top of the leaderboard this season was none other than <TT.Name>
                GebutterteWurst
            </TT.Name>! Other honorable mentions were <TT.Name>
                LaMDaKis
            </TT.Name> in 2nd and <TT.Name>
                Callum
            </TT.Name> in 3rd. Hopefully this event will be returning next season after the release of 2.0, but if you want to stay updated on the latest information, make sure you grab the role from #military-info!
        </p>
        <h2>
            Main Stage: An Interview with Geb 🌎
        </h2>
        <p>
            Recently I had the opportunity to reach out to someone who quickly became a core part of our guild, none other than GebutterteWurst. To celebrate his 1 year anniversary with ANO, I’m doing my best to get this volume released on the exact day (but hey I’m only human and it might be late, let’s see if I can do it).
        </p>
        <p>
            Geb and I haven’t had much of an opportunity to establish much connection, which is why I thought it only fitting to send him some thought-provoking questions – this way both myself and all of you can get to know him just a bit better! So without further adieu, let’s learn something about one of our newest councilors! Text in italics are my own thoughts or responses.
        </p>
        <TT.QnA>
            <TT.Q>
                What’s your favorite thing to do on Wynn?
            </TT.Q>
            <TT.A>
                “From the moment I first joined Wynncraft, I have really enjoyed doing quests, and still do now, even after having completed every quest over 10 times. Just recently, I did all the quests on another class in an optimal order to minimize walking distance, which was incredibly fun.”
            </TT.A>
            <TT.A>
                <em>
                    Quests are one of my favorite parts of Wynn too, even ROL3. It’s a shame you can get through them all so fast and new quests don’t come often.
                </em>
            </TT.A>
            <TT.Q>
                How has your experience in ANO been and how does it compare to other guilds you’ve been in?
            </TT.Q>
            <TT.A>
                “My time in ANO has always been filled with lots of joy and fun; even in hard times or when I have to deal with people or topics I don't particularly like, I tell myself that dealing with hardships is part of the journey.”
            </TT.A>
            <TT.Q>
                How has ANO enhanced (or detracted from, though seems unlikely) your time playing Wynncraft?
            </TT.Q>
            <TT.A>
                “There is too much to mention here, but probably the biggest thing is ANO events. Besides events being very fun, they also made me rich, leading to what is probably more than half of my net worth being from some event.”
            </TT.A>
            <TT.A>
                <em>
                    I am definitely not taking notes here and saving them for later. For anyone wondering how to make money quickly, events are fun and almost always have sizable LE prizes!
                </em>
            </TT.A>
            <TT.A>
                <em>
            editor's note: android_zx (former member) doubled my net worth. Then the llamas doubled that again.
                </em>
            </TT.A>
            <TT.Q>
                What made you want to join ANO in the first place?
            </TT.Q>
            <TT.A>
                “There isn't any special story to tell here; like most people I saw an advertisement. I think it was a shout from Cal and I decided to apply.”
            </TT.A>
            <TT.A>
                <em>
                    My decision to apply was a bit more complex than this, but that’s a story for another time.
                </em>
            </TT.A>
            <TT.Q>
                You’ve been in the guild about a year now. What’s one word you’d use to describe ANO?
            </TT.Q>
            <TT.A>
                “Nonchalant.”
            </TT.A>
            <TT.Q>
                As one of the newest members of parliament, is there anything you thought would change/be different after becoming councilor? Or are your experiences close to what you thought they would be?
            </TT.Q>
            <TT.A>
                “It's mostly like I expected it would be – a lot of things to do, some responsibility, but also a lot of fun.”
            </TT.A>
            <TT.A>
                <em>
                    I was hoping you’d say fun! That’s the most important part!
                </em>
            </TT.A>
            <TT.Q>
                What’s your favorite thing about being in ANO so far?
            </TT.Q>
            <TT.A>
                “It really doesn't boil down to one singular thing; everything about ANO is great and I love that.”
            </TT.A>
            <TT.Q>
                If you could change one thing about ANO or the guild community as a whole, what would it be?
            </TT.Q>
            <TT.A>
                “I feel like some people in the guild community should remember that at the end of the day, Wynncraft is still just a game and should take it less seriously.”
            </TT.A>
            <TT.A>
                <em>
                    Hard agree, and I think this is true across MMOs as a whole. Some people really like to get worked up over a virtual world I suppose.
                </em>
            </TT.A>
            <TT.Q>
                What other games have you been enjoying recently? Or maybe a hobby outside of gaming altogether?
            </TT.Q>
            <TT.A>
                “Just recently I started playing BloonsTD Battles 2 again. I had played it for a while just after launch, but back then it had a lot of issues. Luckily, most of the major issues got resolved and now it's actually a lot of fun!”
            </TT.A>
            <TT.A>
                <em>
                    Is this my call to try BloonsTD Battles? Maybe. I do love Bloons as a franchise.
                </em>
            </TT.A>
            <TT.Q>
                I never got your input on the 2.0 update and the Beta in my other interviews. Summarize your thoughts/concerns/questions/et cetera here.
            </TT.Q>
            <TT.A>
                “The new quests are cool, even if some of them are more movie-like, such as RoL 3. A lot of the abilities are still very unbalanced and some are completely broken, and the new raid is cool but a lot easier than I expected it to be. I really like the new Kandon-Beda and Nesaak designs, even if they somewhat remind me of EO. I know they redesigned Thesead as well, but I have yet to go there. I guess we will see how things go when they release the update soon!”
            </TT.A>
            <TT.A>
                <em>
                    And the beta should be available to all players by the time this comes out! GOGOGO check it out!
                </em>
            </TT.A>
            <TT.Q>
                Is there anything else you wanted to mention about Wynn, Titans Valor, or anything else?
            </TT.Q>
            <TT.A>
                “Among Us”
                <TT.Image src="/news/vol5/amogus.webp" alt="amogus" width={2} />
            </TT.A>
            <TT.A>
                <em>
                    I looked at a trash can and I thought “that’s a bit sussy!!!!” (iykyk)
                </em>
            </TT.A>
        </TT.QnA>
        <p>
            And thus concludes this interview! If there’s anything this one has taught me, it’s that Geb is a pretty chill dude, so don’t be afraid to say hi in chat! Special thank you to Geb for letting me send him some questions.
        </p>
        <h4>
            Member Shout of the Publication
        </h4>
        <p>
            <TT.Name>
                Nimbuh
            </TT.Name>, for contributing over 100 bombs to our bomb party recently
        </p>
        <TT.TriviaFact>
            Messier 51, or the Whirlpool Galaxy, was the first celestial object to be classified as a spiral galaxy. Here’s an image I took of it back in July!
        </TT.TriviaFact>
        <TT.Centered>
            <TT.Image src="/news/vol5/messier.png" alt="Messier 51" />
        </TT.Centered>
        <h3>
            Closing Remarks
        </h3>
        <p>
            As always, thank you, reader, for making it down to this part. Y’all’s enjoyment of the Times is what keeps me writing and bringing you the latest ANO and Wynn news. I’m hoping 2.0 will spur some fun prompts for our main stage, but I can’t wait to see what’s coming down the pipeline!
        </p>
        <p>
            As always, thank you to Aezuh, editor for the times, and to my fellow Brilliance members who usually end up proofreading for spelling/grammar mistakes as an excuse to dunk on me.
        </p>
        <p>
            If there’s something you’d like to see in the Times in the future, feel free to drop me a DM so we can talk! Or if you might be interested in helping write for the Times in the future, send in a Brilliance application today and let ‘em know I sent ya!
        </p>
        <p>
            Happy 1 year with ANO Geb!
        </p>
        <TT.Signature>
            bitesizedbee4 &lt;3
        </TT.Signature>
    </>);
}

"use client";

import Link from 'next/link';
import Header from '../../components/Header';
import { Swiper, SwiperSlide } from '../../components/compat/Swiper';
import { usePathname } from 'next/navigation';
import newsData from '../../data/newsData.json';

export default function Layout({children}) {
    const volLink = usePathname().split("/").at(-1);
    const currentVolume = newsData.find(x => x.link === volLink);
    const currentVolNum = newsData.findIndex(x => x.link === volLink) + 1;

    const volumeListing = newsData.map((volume, volNum) => {
        return <SwiperSlide key={`header-${volume.link}`}>
                <Link href={`/news/${volume.link}`} className={`${volLink === volume.link ? "bg-gradient-to-r from-zinc-800 via-zinc-700 to-zinc-800 border-zinc-600" : "hover:bg-zinc-800 focus:bg-zinc-800 border-transparent"} border-2 transition-all duration-300 mx-3 my-2 h-8 rounded-md flex items-center`} draggable="false">
                <h3 className="w-full text-center font-header font-bold text-sm m-0">
                    {`Volume ${volNum + 1}`}
                    <div className="w-full m-auto h-0">
                        <div className={`${volLink === volume.link ? "w-1 h-1 border-4 border-transparent border-t-white" : "w-0 h-0"} m-auto transition-all duration-300`}>
                        </div>
                    </div>
                </h3>
            </Link>
        </SwiperSlide>
    });

    const breakpoints = {
        "0": {slidesPerView: "1.4"},
        "400": {slidesPerView: "2"},
        "550": {slidesPerView: "3"},
        "700": {slidesPerView: "4"},
        "1000": {slidesPerView: "5"},
        "1400": {slidesPerView: "7"}
    };
    return (<>
        {/* Subheader */}
        <div className="mt-4 m-auto max-w-screen-2xl sticky top-20 z-30">
            <div className="w-2/3 m-auto relative">
                <Header width="100%" height="3" hasFlares="true">
                    <nav className="h-full">
                        <Swiper breakpoints={breakpoints} centeredSlides={true} grabCursor={true} initialSlide={currentVolNum - 1}>
                            {volumeListing}
                        </Swiper>
                    </nav>
                    <div className="w-full h-full absolute left-0 bottom-0 pointer-events-none bg-gradient-to-r from-zinc-950 dark:from-zinc-900 to-50% md:to-30% z-10">
                    </div>
                    <div className="w-full h-full absolute right-0 bottom-0 pointer-events-none bg-gradient-to-l from-zinc-950 dark:from-zinc-900 to-30% z-10">
                    </div>
                </Header>
            </div>
        </div>

        {/* Article content */}
        <main id="main" className="mt-8 mb-32 max-w-3xl m-auto px-4">
            <h3>
                Volume {currentVolNum}
            </h3>
            <p className="my-1">
                {currentVolume.publicationDate}
            </p>
            <h1 className="mt-12">
                Titan Times, A New Edition 📰
            </h1>
            <blockquote className="mt-4">
                {currentVolume.preamble}
            </blockquote>
            <article className="mt-16">
            </article>
            {children}
        </main>
    </>);
}

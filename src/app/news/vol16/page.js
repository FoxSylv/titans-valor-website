import * as TT from '../NewsComponents';

export default function Page() {
    return (<>
        <h2>
            Recent Happenings 📅
        </h2>
        <p>
            To kick things off, the long anticipated 2.1 update finally came, and with it brought countless changes that caught even the most seasoned players off guard! 
        </p>
        <p>
            Firstly, most major changes came from raids. The raiding system had completely changed including its UI. There is now a new silverbull division that you must register before starting your first ever raid. Similarly to the lootrun system, loot pools that changed weekly were introduced as well as ranks, with the lowest being Rookie I and highest being Grandmaster, these ranks allow you to gain more pulls by giving you access to “Gambits” which are personal challenges an individual can enable throughout the raid in exchange for more pulls. Furthermore, the ranks also unlock slots for the newly introduced “Aspects” buffs which boosts an ability in your ability tree if activated. Aspects can be found in these weekly rotated loot pools.
        </p>
        <p>
            Other changes, which I won’t dive too deep into are; the introduction to the big boss Annihilation (nicknamed Annie) which introduced new mythics through a chest shuffle system (similar to the loot run pool) after defeating Annie, the new UI, the removal of soul points (yay?!), teleportation scroll revamp of 3 charges per account (recharge 1 by waiting 10 minutes), equipment nerfs, challenging mobs across all contents (including altars and Legendary Island), sub-class nerfs, the new loot run camp in COTL, new music, new mobs (birbs), revamped dungeons, amongst lots of other changes!
        </p>
        <p>
            Back to ANO news, I’m thrilled to announce that ANO has achieved yet another monumental milestone, reaching level 107 from level 105 since the last edition! Yup! You heard that right, we went over 2 levels in just a short amount of time. Huge round of applause to all the titans who made this possible! (108 by next edition???)
        </p>
        <TT.Centered>
            <TT.Image src="/news/vol16/lvl106.png" alt="Guild level 106 announcement" />
            <TT.Image src="/news/vol16/lvl107.png" alt="Guild level 107 announcement" />
        </TT.Centered>
        <p>
            Buckle up soldiers, it’s time for Community Promotions!
        </p>
        <p>
            A new titan has emerged! Let's all give a massive wave of applause to <TT.Name>Talkair</TT.Name> (me??) for his epic new promotion! Once again, let us all congratulate this guy for his new role in ANO. I am excited to see what the future may bring for him (well, me).
        </p>
        <p>
            New Artificers also entered the scene! Huge congratulations to <TT.Names>__Martyy, Hibikies,  PiglinRose, and takowako</TT.Names> for receiving this well-deserved promotion. We all look forward to your work within ANO and once again congratulate you on this milestone!
        </p>
        <p>
            Oh? Dozens of warlocks also emerged! Congratulations to <TT.Names>Takowako, ConcaveTriangle, Gtloverboy, 05Bucketlover, PiglinRose, ShamanOTP, Suryo, ibu1104, Sniperman127, Eulaw, cw_122, SoulSoil, _HalfDollar, LegoMonkey19, MFLR5, and Joakh</TT.Names> for earning yourselves a solid promotion in ANO. We look forward to seeing you all climb the ranks of ANO in the near future.
        </p>
        <h3>
            What's in the Cupboard?: ANO Cabinet Review
        </h3>
        <h4>
            Spirits
        </h4>
        <p>
            Looks like the spirits crew outdid themselves this time with multiple big events, and tons of giveaways!
        </p>
        <p>
            The number 1 event hoster for the last few weeks was the epic <TT.Name>Overfears</TT.Name>! Hosting a total of 3 events, Overfears went above and beyond, consistently delivering fun and engaging events that kept bringing our community closer. The events he hosted were:      
        </p>
        <ul>
            <li>
                Jackbox: where players engaged in multiple entertaining minigames fighting for the top winner. The minigames included: Fibbage 3, Survive the internet, Patently stupid, Trivia murder party 2, and Push the button.
            </li>
            <li>
                Minecraft Bingo, as the name suggests is a twist to the classic bingo game we all know and love. It required players to split into 4 teams and get items across the Minecraft world to get bingo! The event had 3 segments; Overworld bingo (regular), Overworld bingo (with a twist), and Nether bingo (with pvp). The manhunt had 2 teams, one team aimed to hunt the other team, and the other aimed to kill the enemy team before they got bingo.
            </li>
            <li>
                Death run, where one team of players had to run across a course containing parkour mobs etc, and another (smaller) team triggered traps along the course to kill the runners. Trappers only got points if they killed all the players!
            </li>
            <li>
                Hide and seek, where players had the chance to revisit their childhood memories with a Minecraft twist. Hiders were instructed to hide inside a zone, outlined by a red line as illustrated on the wynn-map by Overfears. Rules instructed them to not run from seekers when caught, they were only legible to participate if they were in a party, and they could not hide in quest-locked areas.
            </li>
        </ul>
        <p>
            Moving on, let’s all give a huge wave of applause for <TT.Name>Lamplesspost</TT.Name> (formerly thatlamppostguy) who hosted a wynncraft kahoot event for the wynn nerds in the ANO community! Three rounds of kahoot were held, each with different sections of wynn knowledge. The sections were: Wynncraft Lore, Wynncraft Item Knowledge, and Wynncraft General Knowledge. To continue, he even surprised us with a bonus round!
        </p>
        <TT.Centered>
            <TT.Image src="/news/vol16/kahoot.png" alt="Kahoot logo" />
        </TT.Centered>
        <p>
            Sailing onwards, two Flash GeoGuessr games were hosted in the past few weeks. One was hosted by <TT.Name>MrRickroll</TT.Name> and the other by <TT.Name>Kebabdoner</TT.Name>. If you don’t know yet, GeoGuessr is a game where you are put in a random place in the world and have to figure out where you are. The closer your guess, the more points you get. Players only had to follow two simple rules: No Googling, and they had to make sure they put their Ign as their user when joining the game. 
        </p>
        <p>
            Looks like our final destination within spirits is the scavenger race event hosted by <TT.Names>Inuke__ and __Martyy</TT.Names>. There were 12 spots scattered across the map, and players had only 1 hour to find them via the help of pictures sent in their DMs. They all started with the same picture, and every time they found the depicted area, they received the next location. This was repeated until they found the 12th and final spot.
        </p>
        <TT.PhotoGallery>
            <TT.Image src="/news/vol16/scavengerhint.png" alt="Scavenger hunt clue" />
            <TT.Image src="/news/vol16/scavengersolution.png" alt="Scavenger hunt solution" />
        </TT.PhotoGallery>
        <p>
            Moving on- Oh! Looks like we missed a navigation point sailors. Giveaway news!! This time we had a total of  1,408le (22stx) of giveaway money! 443le (7stx) from <TT.Name>Zepart</TT.Name>, 64le (1stx) from <TT.Name>Asoart</TT.Name>, and 128le (2stx) from <TT.Name>Inuke__</TT.Name>, 64le (1stx) from <TT.Name>anAlarmingAlarm</TT.Name>, 64le (1stx) from <TT.Name>Hibikies</TT.Name>, 64le (1stx) from <TT.Name>Eulaw</TT.Name>, 192le (3stx) <TT.Name>Gebuttertewurst</TT.Name>, 192le (3stx) from <TT.Name>LaMDaKiS</TT.Name>, and 192 (3stx) from <TT.Name>Kloweechu</TT.Name> respectively.
        </p>
        <p>
            We also had another giveaway of a green overall Hero by <TT.Name>Formatives</TT.Name>! (thank you formatives -Andrew)
        </p>
        <p>
            A huge wave of applause to all the spirit members who remain to host these events and keep our community engaged. Remember to react to the events role in the #role-react channel to keep updated for these fun future events and giveaways!
        </p>
        <h4>
            Brilliance
        </h4>
        <p>
            Onto our next navigation point: into the cabinets of brilliance! The brilliance crew, as usual, has been cooking up out-of-this-world innovations with the bots in ANO. 
        </p>
        <p>
            It’s no secret that with the new update, it brought about many bugs. These bugs and “updates” obliterated some of our bots’ functionalities. But, lucky for us, the brilliance crew came in clutch and was already hard at work to fix these issues. Commands which had previously broken down were namely; the -coolness command, the territory tracking bot, the -sus command, some -p [username] commands, amongst many others that fell victim to rekindled.
        </p>
        <p>
            Sailing onwards from resolved bugs, let’s head towards the brilliant invention of the -graids command! This invention, as the name suggests, tracks player’s guild raids number and works like other commands such as -warcount. I told you brilliance keeps cooking up brilliant stuff (it’s in their name after all. Wave of applause to the brilliant <TT.Name>Aezuh</TT.Name> for developing such a brilliant command!
        </p>
        <TT.PhotoGallery>
            <TT.Image src="/news/vol16/graid1.png" alt="Example command result" />
            <TT.Image src="/news/vol16/graid2.png" alt="Example command result" />
        </TT.PhotoGallery>
        <p>
            Our last point in brilliance is the completion of the ANO map on our very own website! Plans for the map were previously mentioned in the last edition of the titan times, and it’s safe to say we succeeded in completing it. Thanks a bunch to <TT.Name>Foxsylv</TT.Name> for all her efforts in creating this! You can check out the map at <a href="https://titansvalor.org/map">https://titansvalor.org/map</a>.
        </p>
        <TT.Centered>
            <TT.Image src="/news/vol16/map.png" alt="ANO's Wynncraft map" />
        </TT.Centered>
        <p>
            Once again, thank you to the brilliance crew for always being so resilient in fixing the errors that wynncraft splashes onto us. Don’t forget to apply for brilliance if you have any experience in coding, or want to join our newsletter team for the times!
        </p>
        <h3>
            Military Updates ⚔️
        </h3>
        <p>
            Do you hear that? It’s war horns!
        </p>
        <p>
            We once again bid farewell to another war season, Season 19. This time the first place went to <TT.Name>anAlarmingAlarm</TT.Name> who earned herself the title of “Champion of the Ocean” in that season’s Ocean Trials, followed by <TT.Name>Talkair</TT.Name> for the 2nd place, and <TT.Name>Inuke__</TT.Name> for the 3rd! Let’s give anAlarmingAlarm a huge wave of applause for doing a whopping, and jaw-dropping total of 5159 wars!
        </p>
        <p>
            Furthermore, we also bid farewell for season 20! <TT.Name>anAlarmingAlarm</TT.Name> once again claimed the champion of the ocean title with an astonishing 2376 wars. In 2nd place was <TT.Name>Hibikies</TT.Name>, followed by <TT.Name>lamplesspost</TT.Name> in 3rd! Big round of applause to the soldiers who keep contributing to ANO’s rise in the global military of wynn.
        </p>
        <p>
            Sailing on, it is without a doubt that with rekindled, it brought about changes to the territory names across wynn. Some of the names that changed in our claim were the previous dead island territory names, now individually known as Derelict Mansion, Lifeless forest, Dreary Docks, and Disturbed crypt. Do you still remember the old names of the territories that had a name change?
        </p>
        <p>
            Hoist the anchor sailors! Time for promotions!
        </p>
        <p>
            A huge round of applause for our new Strategists: <TT.Name>__Martyy</TT.Name> (also known as farty), the stunning <TT.Name>__Inuke</TT.Name>, and the epic <TT.Name>Howl888</TT.Name>! With new strategists, ANO’s military ability against attacker guilds continues to strengthen. I, and the other soldiers are expecting amazing things from you all.
        </p>
        <p>
            Hold on tight sailors, time for this volume’s Main Stage!
        </p>
        <h2>
            Main Stage: A Look at Wynncraft’s Many Areas
        </h2>
        <blockquote>
            Written by Lamplesspost
        </blockquote>
        <p>
            Hello everyone! I’m Lamplesspost and welcome to the main stage of this edition of Titan Times. Wynncraft has many different areas, each with their own styles and themes. Today I’m going to be taking a short but in depth look into their builds, atmosphere, and more. So without further ado, let us begin! 
        </p>
        <h3>
            Wynn Plains
        </h3>
        <p>
            The Wynn Plains are the first area encountered by the player, and to this day, one of, if not, my favourite area in the game. The way you can really see the impact of the Corruption on the land, the way it feels almost weary, really pulled me into the world and the lore when I first began playing. While there are many locations that are far more eye-catching and interesting, the atmosphere of the Wynn Plains alone will always make it perhaps my favourite area to return to. 
        </p>
        <h3>
            Time Valley
        </h3>
        <p>
            While technically a part of the Wynn Plains, I would be doing a disservice if Time Valley didn’t get a section of its own. It too is one of my favourite areas, the home of a long gone civilization. The builds really sell the mystery of the area, and it's made better by their being so many unanswered questions, some of which are never explained. An ancient civilization should feel mysterious, and it's fitting that their long forgotten home feels that way too.
        </p>
        <TT.Centered>
            <TT.Image src="/news/vol16/timevalley.png" alt="Time Valley with shaders" />
        </TT.Centered>
        <h3>
            Savannah
        </h3>
        <p>
            The Savannah is ultimately a transition period between the Wynn Plains and the Almuj desert, and it does kind of show. There’s just not much to do here, and the atmosphere isn’t really anything special. Ternaves and Bremminglar are definitely nice additions to the area though, especially after the rebuild in 2.1.
        </p>
        <h3>
            Early Ocean
        </h3>
        <p>
            I’m going to be grouping the early level Ocean areas together for this, purely because there’s not really much too them build wise or content wise. Mage Island is definitely a standout in terms of builds, it really does feel like a home of recluse sages. Seavale Reef is also a pretty cool area, though it’s sad it doesn’t have any real content. 
        </p>
        <h3>
            Almuj Desert and Rymek Mesa
        </h3>
        <p>
            The Almuj desert is another place home to an ancient civilization, and while not quite as awe inspiring as Time Valley, it’s by no means a bad area. You do get the feel of it being desolate and inhospitable, though this is somewhat limited by the small size of the area- an unfortunate necessity for the sake of gameplay. The Rymek Mesa to the south of the desert is also a really cool area, I'm personally a huge fan of the builds and the way it almost feels like somewhere an old western would be set. 
        </p>
        <h3>
            Nesaak Forest
        </h3>
        <p>
            The Nesaak Forest is another one of my favourite areas, again mostly on the atmosphere alone. I’m a huge fan of the way the place feels kind of… sad. It’s mostly free from corruption, but at the cost that its citizens are forced to carve out an existence in the freezing wastes. The Twain Valley isn’t technically part of the Nesaak Forest, but I really couldn’t go without mentioning it. You immediately get the sense that something tragic happened here, even before you know what eventually befell the Twains.
        </p>
        <TT.Centered>
            <TT.Image src="/news/vol16/nesaakforest.png" alt="Nesaak Forest with shaders" />
        </TT.Centered>
        <h3>
            Llevigar Plains
        </h3>
        <p>
            The Llevigar Plains are the player’s first introduction to Gavel, and sadly they’re not anything too special. Ultimately, the landscape itself isn’t too interesting, and while the fairy tale-ness and the orc camps is nice enough, the atmosphere just isn’t good enough to make up for the boring landscape.
        </p>
        <h3>
            Icy Islands
        </h3>
        <p>
            The Icy Islands are one of the more interesting areas in the Ocean in terms of lore, however the builds are pretty outdated, and don’t really reflect the story too well in some cases. The Nodguj and Dujgon Nations have supposedly been at war for quite some time, yet there is basically no impact on the land that we see. It really doesn’t feel like a battleground at all.
        </p>
        <h3>
            Olux Swamp
        </h3>
        <p>
            The Olux Swamp is the first place in Gavel that’s affected by the decay. It certainly feels gloomy enough, and you do see the effects of the decay on the land. The overall atmosphere, though, I think is just done far better in the other decay areas, which we’ll get to in a bit.
        </p>
        <h3>
            Troms Jungle and Dernel Jungle
        </h3>
        <p>
            Wynn’s Jungles are overall pretty good areas, they feel overgrown and full of life, both hostile and friendly, but not too much of a pain to navigate. I also like how they feel kind of mysterious, like there’s so much you just don’t know. I will say the giant fortress of Troms does feel ever so slightly out of place to me, but I can’t really complain too much, as Troms is a very well built city and one of my favourites.
        </p>
        <TT.Centered>
            <TT.Image src="/news/vol16/jungle.png" alt="Jungle with shaders" />
        </TT.Centered>
        <h3>
            Zhight Island
        </h3>
        <p>
            Funny seeds.
        </p>
        <h3>
            Skien's Island
        </h3>
        <p>
            The home of the disgraced general Skien, a ruined fortress guarded by undead hordes. It’s a pity that all that’s really here is a boss altar. Skien could’ve been much cooler in a dungeon in my opinion, but alas there is already a level 50ish dungeon in the form of Undergrowth Ruins.
        </p>
        <h3>
            Volcanic Isles
        </h3>
        <p>
            For an Island, there’s a reasonable amount to do on the Volcanic Isles, a boss altar and a quest, definitely a lot more than some other places in the Ocean. Sure, it’s not masses to do, but I think that it’s a nice amount of content. The builds are perhaps a little outdated, but nothing too awful, and something to be expected with an older game like Wynncraft.
        </p>
        <h3>
            Maro Peaks
        </h3>
        <p>
            Maro Peaks is perhaps the least interesting area in the game. If I had to rank them, it would probably come last. There are some quests on Maro Peaks, more than some other Islands, in fact. Yet none of them are really anything special, and the fact that you need an item from a completely unrelated quest to even access it, is really stupid. What’s worse is it’s an armour piece, so putting it on can break a lot of builds.
        </p>
        <h3>
            Pirate Town and Lost Atoll
        </h3>
        <p>
            I'm grouping the two Pirate-y Islands together, as they’re pretty similar in aesthetic. I have to say, I’m definitely a fan of the pirate aesthetic overall, and these two islands do a pretty good job of portraying it. What’s more, the quests around these Islands are actually pretty decent, and Galleon’s Graveyard is a solid dungeon too.
        </p>
        <TT.Centered>
            <TT.Image src="/news/vol16/galleonsgraveyard.png" alt="Galleon's Graveyard with shaders" />
        </TT.Centered>
        <h3>
            Decay Forests
        </h3>
        <p>
            The Decay Forests are another of my favourite areas. They’re probably the darkest areas in the game, short of the Silent Expanse. You can really see how the decay has scarred the land, turning into a withered wasteland. The sense of grim foreboding you get just walking around always stuck with me, the way you can almost feel the land’s suffering. It really gets across the impact of the Decay and Dern in general- a once beautiful land now drained away to a husk of its former self.
        </p>
        <h3>
            Dead Island
        </h3>
        <p>
            Sticking with the theme of dark places, let’s talk about Dead Island. Dead Island is another place warped by darkness, though in a somewhat different fashion. It’s definitely not the worst Island, there is a quest there after all, if an alright one, but it does kind of feel like a bit of a rehash of the Decay.
        </p>
        <h3>
            Light Forest
        </h3>
        <p>
            The Light forest is one of the first areas in Gavel that’s still truly under the influence of the Light. It’s the most colourful place that the player’s been to, which really sets up the Realm of Light itself, somewhere the Light Forest is ultimately a gateway to. The Aldorei Valley, the home of the elves, is another really nice area, with some of the best builds in the entire game in my opinion.
        </p>
        <TT.Centered>
            <TT.Image src="/news/vol16/lightforest.png" alt="Light Forest with shaders" />
        </TT.Centered>
        <h3>
            Realm of Light
        </h3>
        <p>
            The Realm of Light, a group of floating islands that look like a rainbow exploded on them, and what’s more 100% guaranteed to cause eye damage. Which, when you think about it, does kind of make sense. I also like how the Realm of Light is actually pretty small, showing how the Light is in a state of decline due to the war against Dern. Pretty neat visual storytelling, even if it is later explicitly stated by Lari.
        </p>
        <h3>
            Gylia Plains
        </h3>
        <p>
            Gylia Plains are kinda weird. Not that that’s a bad thing. After all, it’s meant to be a landscape warped by the cosmic magic of the two meteors that crashed there. It’s unlike anywhere else in the game, which really sells the bizarreness of cosmic magic.
        </p>
        <h3>
            Canyon of the Lost
        </h3>
        <p>
            The Canyon of the Lost is one of the largest areas in the game, with a fairly basic but cool design. While the constantly shifting mountains can’t really be added into the actual game, the concept of it is cool enough, and the builders definitely tried their best to make it feel as maze-like as possible. It’s certainly pretty easy to get lost without a map. I also like how the canyon does a great job of setting up its ultimate boss- The Colossus. aka what’s caused the shifting paths and crumbling mountains in the first place.
        </p>
        <h3>
            Corkus Island
        </h3>
        <p>
            It might be considered odd that my favourite area on the steampunk island is well, not the steampunk bit. I just find the druidic aesthetic of the Avos far more interesting. Although, part of the reason for that is admittedly because of the contrast between the two cultures.
        </p>
        <p>
            The builds do a really good job of showing this, with the Avo houses being made of natural materials such as straw and wood, whereas Corkus City is composed almost entirely of brick and stone. It makes the conflict really compelling, at least in terms of visuals. Whether or not the actual story lives up to this I’ll keep out of the review.
        </p>
        <h3>
            Molten Heights
        </h3>
        <p>
            The Molten Heights are a range of fiery volcanoes carved out by the Doguns, which is a pretty cool origin in of itself, but is arguably made even cooler by the fact that it later became a battleground for some truly epic battles between the Dwarves and Doguns. I really like the fact that you can actually see the impacts of the war on the landscape itself, such as the Freezing Heights, an entire area of the Heights frozen over by an Ice Drake.
        </p>
        <h3>
            Sky Islands
        </h3>
        <p>
            The Sky Islands are another area with a really cool origin, a land fractured by the throes of the Ahms Colossus. The landscape itself is also pretty cool too, an archipelago of floating islands. It’s not anything crazy unique, but I’m always a sucker for floating islands. I’m especially a fan of the void region, a whole area suspended in the void below the main islands, a remnant of the land before it fractured that has been twisted and distorted over the centuries. The builds there are damn good too.
        </p>
        <h3>
            The Silent Expanse
        </h3>
        <p>
            Well, we’re finally here, the highest level area in the game, and definitely one of the scariest and most horrifying. The builds do an amazing job of showing how the Darkness has warped and tainted the landscape. Everything is malformed and disfigured. Even the mountains are dark and twisted. Not to mention the individual areas. A forest with literal eyeballs growing on trees is so utterly bizarre and horrifying, yet it fits the aesthetic perfectly. The Silent Expanse is, after all, the Road to Dern, and it does an excellent job of setting it up.
        </p>
        <TT.Centered>
            <TT.Image src="/news/vol16/silentexpanse.png" alt="Silent Expanse with shaders" />
        </TT.Centered>
        <p>
            Congratulations! You’ve made it to the end! This has been the longest main stage I’ve written so far by quite a bit, so I can only hope you haven’t all fallen asleep reading it. This is all from me for this edition of Titan Times, so thank you very much for reading, and I hope you enjoyed it! 
        </p>
        <h3>
            Member Shoutout of the Publication
        </h3>
        <p>
            Shoutout to <TT.Name>Lamplesspost</TT.Name> for his third publication of the Main Stage for 3 times in a row! 
        </p>
        <TT.TriviaFact>
            Did you know: Australia is wider than the moon? Yup! While the moon sits at 3,400 km (2,113 miles) in diameter, Australia’s diameter from east to west is almost 4,000 km (2,485 miles)!
        </TT.TriviaFact>
        <h3>
            Closing Remarks
        </h3>
        <p>
            This concludes the end of Volume 16 of the Titan times! Thank you for once again trusting me as your captain for this journey. See you next time, Titans!
        </p>
        <TT.Signature>
            -Talkair
        </TT.Signature>
    </>);
}

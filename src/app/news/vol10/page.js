import * as TT from '../NewsComponents';

export default function Page() {
    return (<>
        <h2>
            Recent Happenings 📅
        </h2>
        <p>
            In between the release of our last volume and this one, the 2nd annual Festival of the Heroes event brought additional flair to the usual gameplay for a few weeks. While I’m sure you all had a chance to check out Festival Detlas, it’s still worth the recap!
        </p>
        <TT.PhotoGallery>
            <TT.Image src="/news/vol10/detlas-day.png" alt="Festival Detlas during the day" />
            <TT.Image src="/news/vol10/detlas-night.png" alt="Festival Detlas at night" />
        </TT.PhotoGallery>
        <TT.Centered>
            <p>
                Photos courtesy of KidOfCubes
            </p>
        </TT.Centered>
        <p>
            Special festival crates were available for a limited time, providing players with a wide range of unique cosmetics. Crates could be obtained for free by completing daily and weekly objectives. There were also the darts and color wheel activities, as well as the <strong>Call to Heroism</strong> minigame! Players were able to earn festival tokens to obtain unique trinkets and fun items. And let’s not forget the scavenger hunt! Each day of the festival, a new box was hidden somewhere around the map for players to find after being provided a hint to the location (or just looking up the coordinates on the wiki if you’re me). Each box found provided players with reward vouchers containing a variety of items. By reaching certain milestones of boxes found, players were able to obtain mildly useful items. Festivals like this come around every couple of months, so keep your eyes out in the future for the next one.
        </p>
        <p>
            Around the time this gets posted, it will be Wynncraft’s 10th anniversary! Crazy to think this game has been around a whole decade now. It’s changed a lot since those early days (RIP my pre 1.14 Fierte that went into the void). As of right now, it’s unclear whether or not there will be any in-game celebration for this, but for now you can check out Salted’s post on the Forums for some fun BTS pictures over the years. Check it out <TT.Link href="https://forums.wynncraft.com/threads/10-years-of-development.307851/">
                Here
            </TT.Link>!
        </p>
        <TT.Centered>
            <TT.Image src="/news/vol10/devmap.png" alt="Map from early development" />
        </TT.Centered>
        <p>
            Let’s round off this section with all of last month’s promotions, shall we?
        </p>
        <TT.Indented>
            <p>
                <TT.Names>
                    Vampellao, Silent__Boss, _Ein_Wolf_, ___Apollo__, Long_tata, GamingProf, LordXaverius, Waiter1986, SaintEnder, IndomDoggo, MagischeTaube, ZoSo, Exhinder2295, Fire_209, and CrispperPlanet
                </TT.Names> were all promoted to Sorcerer. Have fun climbing the rest of that ladder!
            </p>
            <p>
                <TT.Names>
                    iThought and OverFears
                </TT.Names> were both promoted to Artificer for their efforts in warring and cabinet work respectively. Great work guys!
            </p>
            <p>
                Last, but certainly not least, a HUGE congrats to <TT.Name>
                    EchoLuu
                </TT.Name> on her promotion to Titan! She has been dedicating so much effort into our warring scene and massively improving her skills as an economiser (all that on top of being an extremely cool guild member in general). I can’t wait to see what else you’ll do!
            </p>
        </TT.Indented>
        <p>
            Congrats to everyone who was recently promoted! I always look forward to seeing new names up here, but having a few big promotions really sweetens the deal.
        </p>
        <h3>
            What’s in the Cupboard?: ANO Cabinet Review
        </h3>
        <h4>
            Spirits
        </h4>
        <blockquote>
            Written by BeGruent
        </blockquote>
        <p>
            Hey Titan’s Valor, it’s everyone’s favorite penguin, BeGruent. This time I will be the one that tells you what has been going on in Titan’s Spirit since last edition.
        </p>
        <p>
            First of all, we had OverFears hosting his first event, Jackbox, which is a collection of games like Push the Button, Monster Seeking Monster, or Survive the Internet. Shortly after that he hosted two additional events, Risk and Minecraft Bingo. Risk is a strategy game where you have to attack your opponents and claim territories.
        </p>
        <p>
            Most of you should know Minecraft Bingo. For everyone that doesn’t here’s how it works: you get a 5x5 sheet and complete a row. The left image shows an example from the practicing round. But there wasn’t only normal bingo, there was also Lockout and Manhunt. While Manhunt is normal bingo with the addition of one team having to kill all players before any other team wins, Lockout is different. Their whole bingo sheet needs to be completed, and as soon as a task is done it’s locked for every team. The goal here is to get the most tasks done, as you see in the right image.
        </p>
        <TT.PhotoGallery>
            <TT.Image src="/news/vol10/bingo1.png" alt="Bingo card" />
            <TT.Image src="/news/vol10/bingo2.png" alt="Lockout bingo card" />
        </TT.PhotoGallery>
        <TT.Centered>
            <p>
                Comparison between lockout and normal bingo
            </p>
        </TT.Centered>
        <p>
            Similar to Minecraft Bingo, I, <TT.Name>
                BeGruent
            </TT.Name>, made a sequel to my first ever event, Wynncraft Bingo. While last time I made it a Lockout, this time it was normal bingo. Additionally, instead of having 1 week to complete all tasks the participants only had 24 hours. Congratulations to the winners, which were <TT.Name override="GebutterteWurst">
                Geb
            </TT.Name>, who got the reward for the first row, <TT.Name>
                linnyflower
            </TT.Name>, who got the reward for being the first and only one to get every task done, and <TT.Name>
                Crisperplanet
            </TT.Name>, who got rewards for not only being the first one to complete 2 rows, but one for 3 and 4 row as well!
        </p>
        <p>
            Last but not least, our friends in [AVO] hosted an amazing Scavenger Hunt event over in Adonis, where you had to find a location using an image, similar to the Geoguesser event that Llamas hosted a while ago. Even though it was pretty difficult, [ANO] managed to secure 3 of 5 rewards. <TT.Name override="GebutterteWurst">
                Geb
            </TT.Name> once again took first place, <TT.Name>
                BlueTheSniper
            </TT.Name> got second, and <TT.Name>
                Aemor
            </TT.Name> secured 4th place. Huge congrats to all of you!
        </p>
        <p>
            That should wrap up the event section for this edition, have fun reading through the rest! If you ever feel the urge to make an event by yourself, don’t hesitate to join the Spirit Cabinet! Applications can be created in <code>
                #guild-info
            </code>.
        </p>
        <h4>
            Brilliance
        </h4>
        <p>
            Brilliance projects take time, okay? This publication is probably the most routine thing that goes on in this cabinet (no shade to my fellow Brilliance members, I jest).
        </p>
        <p>
            The good news is that <TT.Name>
                KidOfCubes
            </TT.Name> is working on a systematic recreation of Wynncraft. If all goes well, it should be similar to Wynncraft but entirely open source. He’ll be releasing a trailer for the project once it’s completed, so look out for that in the (not so) near future.
        </p>
        <p>
            In worse news, anode, our war count mod, seems to be causing crash issues with Wynntils, so our Brilliance team is working to resolve that. There may be a new version of anode soon, so anyone participating in Ocean Trials will want to look out for that if a new version is needed.
        </p>
        <blockquote>
            Still working on the fix :p. You can still use it but it will crash the chat tabs occasionally. (You can restart the chat tabs in Wynntils)
        </blockquote>
        <h3>
            Military Updates ⚔️
        </h3>
        <p>
            Season 11 ends, and season 12 begins! Congrats to all of our war participants last season, as we were able to sinch 3rd place in our season ratings! Congratulations to <TT.Name>
                GebutterteWurst
            </TT.Name> for having the most total wars this season and becoming Champion of the Ocean.
        </p>
        <TT.Centered>
            <TT.Image src="/news/vol10/leaderboard.png" alt="SR leaderboard" />
            <p>
                If I had $1 for every SR point ANO has, I would have $6,042,104
            </p>
        </TT.Centered>
        <p>
            That means that a new season of Ocean Trials has begun! For any new members who want to participate need to have anode installed in order to track your war count. Good luck this season everyone!
        </p>
        <TT.Indented>
            <p>
                As a side note, Ocean Trials rewards will be reverting back to the old system. This means that rewards are given out as a lump sum at the end of the season, instead of weekly like they were this past season. 
            </p>
        </TT.Indented>
        <h2>
            Main Stage: A Critique of Silent Expanse Quests 😮
        </h2>
        <blockquote>
            Written by karapandza
        </blockquote>
        <p>
            In this piece I will be explaining why I think the two quests that introduce the player to the Silent Expanse: A Journey Beyond &amp; A Journey Further fail in many ways in introducing the player to this new region.
        </p>
        <p>
            When I think about what makes the Silent Expanse interesting, I immediately go to one thought. The mystery. And this is what both a Journey Beyond &amp; a Journey Further can use best to their advantage. There is no area on Wynncraft that sheds any drop of semblance to the Silent Expanse. It is completely unique and mysterious. Before doing A Journey Beyond the player only gets a snippet or two of what is behind the gates of the mines. Without any outside knowledge the player does not know anything about what they are venturing into. But all of this mystery is completely lost by one thing. Aledar just simply already knows everything. This is my largest gripe with both quests. It makes them feel less like an adventure into an unknown land and more like a tour guide. The player does not experience the mysteries and wonders of this new land naturally, they are just told what certain things are and how they function.
        </p>
        <p>
            My next point of critique is the factor of importance. This is the first level 100 quest the player can do and throughout the quests one is told repeatedly that this mission is important, but I never got that feeling and I think this stems from one single thing. The people who are telling you this: Elphaba and Aledar. The player has interacted with Elphaba once before in the quest Reclaiming the House and in the case of Aledar the player has not seen him since the tutorial. These are great callbacks to the player's past experiences and encounters but in my eyes are not capable of presenting the importance of the mission. Since in my case I had completely forgotten who both are. But importance is also not present in a particular part of the quest: The player being chosen for the mission. At this point, the player is level 100, incredibly powerful and has been on a lot of adventures (if one decides to do a lot of the quests) yet for some reason you are never chosen to go on this mission. You randomly stumble into a room that plans this great adventure. This is a large missed opportunity to reward the player for reaching level 100 by specifically choosing them for their great feats and accomplishments.
        </p>
        <p>
            I still have one minor point I would like to add but that is not as large as the others. Simply the addition of a completely new element. In the first third of the quest after failing to defeat Black Geist the player goes to a Blacksmith for help. And he introduces a new Element. I do not have an issue with a new Element exclusive to the Silent Expanse, on the contrary, I think this would be an interesting idea but, in the quest, so little is done with it. After defeating the Black Geist, it is completely forgotten and not utilized again (the Wynncraft team may do more with this with the eventual release of Dern but in the current state of the game the Element of Darkness does not add anything).
        </p>
        <p>
            These two quests have the highest potential in the entire game and that is why it is so disappointing to me to see how they ended up. These would be some examples of my most simple suggestions that I believe would go a long way in improving the quests whilst fixing the issues I stated before. The first change I would make is that no one knows anything about the Silent Expanse. It is an entirely unknown area to everyone. This removes my issue with the tour guide feeling I got from the quest. As I have stated before, the largest role playing in favor of the Silent Expanse is the mystery around it and that is why it baffles me why they decided to format the quest the way they did. The next one would be that the player is not recruited by Elphaba in Detlas but instead by the King of Ragni. This is a call back to the player's past and presents the importance much better than Elphaba and Aledar ever could. And it finally rewards the player for all their accomplishments. “No one but you can do this.” It is natural and makes sense in context of the quest. I have many ideas for changes, and these are just examples and if I would list them all out this would enter fanfiction territory. But in my eyes the general idea this quest should strive for is: You have become one of Wynn’s greatest soldiers and now must partake in an adventure into an unknown mysterious land that has not been seen by the eyes of man for centuries to find out what is hiding behind the mountains of Wynn.
        </p>
        <p>
            I am not writing this out of spite. I love this game and I want to see it improve and this is just one of the factors where I see a lot of room for improvement, and I hope that one day these quests reach the potential that they hold.
        </p>
        <h3>
            Member Shout of the Publication
        </h3>
        <p>
            <TT.Name>
                ohza
            </TT.Name>, because he’s cool :D
        </p>
        <p>
            <strong>
                Trivia Fact
            </strong>
        </p>
        <TT.TriviaFact>
            A coffee tree takes around 5 years to reach maturity and begin producing fruit.
        </TT.TriviaFact>
        <h3>
            Closing Remarks
        </h3>
        <p>
            Hey everyone, thank you so much for reading yet another publication of mine. I was so excited to finally showcase work that wasn’t mine. Karapandza sent me his draft a few weeks ago and I loved it. A HUGE thank you to him for his contribution this time. I hope he’ll consider more amazing topics in the future. Another big thank you to BeGruent for volunteering to write the Spirit updates section. He’s been hosting a number of events lately, so it’s only fitting he’d want to talk about all of them with you. As always, thank you to Andrew and the Brilliance team for peer-reviewing the first draft BS I throw at them, y’all are great. I’ll see you all again next month!
        </p>
        <TT.Signature>
            &lt;3 bitesizedbee4
        </TT.Signature>
    </>);
}

# Titan Times Rewrite

A full rewrite of the Titan Times website with many new features.


## Installation

1. Clone the repository
```bash
git clone git@codeberg.org:FoxSylv/titan-times-rewrite.git
```

2. Navigate to the project directory
```bash
cd ./titan-times-rewrite
```

3. Install dependencies
```bash
npm i
```

## Getting Started

First, run the development server:

```bash
npm run dev
```

Then, open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

The page auto-updates as you edit the local files. If it doesn't, try re-running the development server and refreshing the page.
This process is usually required if you change any news article pages.

## Adding news articles

To add more articles, all you need to do is add its  file to [`/src/news/volXX`](/src/news/) and add
its relevent data to [`/src/data/newsData.json`](/src/news/newsData).
It will automatically appear everywhere else on the website, and also be automatically styled. There
are a handful of tags that you can import from the `NewsComponents` file that make creating new articles
much easier. 

**Use the NewsComponents' `<Link>` and `<Image>` instead of the usual HTML `<a>` and `<img>` tags. The modified tags get optimized behind the scenes.**

Also! Don't forget to include good alt text for images! See this site for more info:
[Harvard Alt Text Guide](https://accessibility.huit.harvard.edu/describe-content-images)

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

If you don't have experience with React, take a look at the following resources:

- [React Documentation](https://react.dev/reference/react) - learn about React features and API
- [Learn React](https://react.dev/learn) - an interactive React tutorial

To learn more about TailwindCSS, take a look at the following resource:

- [TailwindCSS Docs](https://tailwindcss.com/docs/utility-first) - learn about TailwindCSS styling
